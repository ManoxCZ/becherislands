/**************************************************************/
/* File : Max_Terrain.h										  */
/* Developed : Manox (manox@atlas.cz)						  */
/* Date : 2.6.2005											  */
/**************************************************************/

#ifndef _TERRAIN_H
#define _TERRAIN_H

#include "Camera.h"
#include "Dock.h"
#include "Texture.h"
#include "Model.h"
#include "Vector.h"
#include "Wave.h"
#include <string>
#include <vector>

struct VisibleQuad
{
	unsigned int Id;
	float Distance;
};

class Terrain
{
private:
	const static int QUAD_SIZE = 32;
	const static float WATER_LEVEL;
	const static float water_sirka;
	const static float water_stretch;
	const static float CELL_SIZE;
	const static float VERTICAL_SCALE;

	Texture _mainTexture;
	Texture _detailTexture;
	const Texture& _waterTexture;
	std::vector<Vector3f> _vertices;
	std::vector<Vector2f> _texCoord0;
	std::vector<Vector2f> _texCoord1;
	std::vector<Vector3f> _normals;
	std::vector<Model> _objects[QUAD_SIZE * QUAD_SIZE];
	unsigned int _quadSize;	
	std::vector<VisibleQuad> _visibleQuads;
	int _width;
	int _height;
	std::vector<SeaWave> _beachWaves;
	double _wavesTotalTime;
	

public:
	std::vector<ModelInstance> Objects[1024];
	std::vector<Molo> Docks;

	Terrain();
	
	float bod_A_x, bod_A_y;
	float bod_B_x, bod_B_y;
	float bod_C_x, bod_C_y;
	float bod_D_x, bod_D_y;
	
	

	float GetHeight(const Vector3f& position) const;
	int quad(float px, float pz);
	bool inside_triangle(float px, float py);
	
	void Update(float deltaTime);
	void Render(const Camera& camera) const;
	void render_ref();
	void UpdateWater(float deltaTime);
	void RenderWater(const Camera& camera) const;
	void RenderTerrain(const Camera& camera) const;
	void RenderObjects(const Camera& camera) const;
	bool loadData(const std::string& file, const std::string& texturka);

private:
	void UpdateVisibility(const Camera& camera);
};

#endif
