#pragma once

#include "Ship.h"
#include "Vector.h"

class Molo
{
public:
	Vector3f Position;
	Vector3f Rotation;
	unsigned int ID;
	Vector2f kol_A;
	Vector2f kol_B;
	Vector2f kol_C;
	Vector2f kol_D;

	Molo(const Vector3f& position, const Vector3f& rotation, unsigned int id, unsigned int typ);
	
	bool CollideWithShip(const Ship& ship) const;
};