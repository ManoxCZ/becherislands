/**************************************************************/
/* File : Max_GUI.cpp										  */
/* Developed : Manox (manox@atlas.cz)						  */
/* Date : 14.9.2005											  */
/**************************************************************/

#include "Dialog.h"

void Dialog::init(float ipx, float ipy, float ikx, float iky)
{
	px = ipx;
	py = ipy;
	kx = ikx;
	ky = iky;
};

void Dialog::init_text(
	const std::string& itext_1, const std::string& itext_2, const std::string& itext_3,
	const std::string& itext_4, const std::string& itext_5, const std::string& itext_6)
{
	text_1 = itext_1;
	text_2 = itext_2;
	text_3 = itext_3;
	text_4 = itext_4;
	text_5 = itext_5;
	text_6 = itext_6;
}

void Dialog::render()
{
	//glDisable(GL_BLEND);
	//glDisable(GL_DEPTH_TEST);

	//HUD.textura_zatext.Bind();
	//
	//glBegin(GL_QUADS);              /* zacatek zadavani ctverce */
	//	glTexCoord2f(0.0,1.0); 
 //       glVertex2i(250,180);			/* poloha prvniho vertexu */
	//	glTexCoord2f(1.0,1.0);
 //       glVertex2i(560,180);		/* poloha druheho vertexu */
	//	glTexCoord2f(1.0,0.0);
 //       glVertex2i(560,350);		/* poloha tretiho vertexu */
	//	glTexCoord2f(0.0,0.0);
 //       glVertex2i(250,350);			/* poloha ctvrteho vertexu */
 //   glEnd();
	//
	//printText("Becher Transport", font_12, 263, 198, 0, 0, 0);

	//printText(text_1, font_12, 260, 230, 0, 0, 0);
	//printText(text_2, font_12, 260, 250, 0, 0, 0);
	//printText(text_3, font_12, 260, 270, 0, 0, 0);
	//printText(text_4, font_12, 260, 290, 0, 0, 0);
	//printText(text_5, font_12, 260, 310, 0, 0, 0);
	//printText(text_6, font_12, 260, 330, 0, 0, 0);
	//glEnable(GL_DEPTH_TEST);
};