/**************************************************************/
/* File : Max_Main.cpp										  */
/* Developed : Manox (manox@atlas.cz)						  */
/* Date : 16.5.2005											  */
/**************************************************************/

#include "Input.h"
#include "Scene.h"
#include <memory>

#define TITLE "MaxTech engine" 

//PFNGLACTIVETEXTUREARBPROC glActiveTextureARB;
//PFNGLCLIENTACTIVETEXTUREARBPROC glClientActiveTextureARB;
//PFNGLMULTITEXCOORD2FARBPROC glMultiTexCoord2fARB;
//
//bool keys[256];
//bool stiskL = false;
//bool esc_key = false;
//unsigned int jazyk = 0;
//bool play = true, contine = false, failed=false;
//bool active = true;
//bool fullscreen = true;

//float hloubka;
//float fov=45.0;
//float near_plane=1;
//float far_plane=100000.0;
//GLfloat scenerotx;

int previousUpdateTime;
Input input;
std::unique_ptr<Scene> scene;
bool isFullscreen = false;

//Level mise;
//Ukol newukol;
//Dialog newdiag;
//std::deque <Dialog> dialogy;
//Titulky kredity;


//bool info=false;
//bool near_veh = false, inside_veh = true, refrakce = false;

//// HUDBA
//Sound hudba_tit;
//Sound hudba_1;
//Sound hudba_2;
//Sound hudba_3;
//Sound samp1;
//Sound sound_tlac;


void Resize(int width, int height) 
{
	if (height == 0) 
	{
		height = 1;
	}

	scene->OnWindowResize(width, height);	
}

void Render(void) 
{
	scene->Render();

	glFlush();

	glutSwapBuffers();
}

void Update(void)
{
	int timeSinceStart = glutGet(GLUT_ELAPSED_TIME);
	float deltaTime = (timeSinceStart - previousUpdateTime) * 0.0001f;
	previousUpdateTime = timeSinceStart;	

	glutForceJoystickFunc();

	scene->Update(deltaTime, input);	

	glutPostRedisplay();
}

void OnKeyDown(unsigned char key, int x, int y)
{
	input.OnKeyPressed(key);
}

void OnKeyUp(unsigned char key, int x, int y)
{
	input.OnKeyReleased(key);
}

void OnSpecialDown(int key, int x, int y)
{	   
	input.OnSpecialKeyPressed(key);
}	   
	   
void OnSpecialUp(int key, int x, int y)
{
	input.OnSpecialKeyReleased(key);

	if (key == GLUT_KEY_F2)
	{
		if (isFullscreen)
		{
			glutReshapeWindow(800, 600);
		}
		else
		{
			glutFullScreen();
		}

		isFullscreen = !isFullscreen;
	}
		
}

void OnJoystick(unsigned int key, int x, int y, int z)
{
	input.OnGamepadPressed(key, x, y, z);
}

void InitOpenGL()
{
	glShadeModel(GL_SMOOTH);

	glDepthFunc(GL_LEQUAL);
	glDepthRange(0.1f, 10000.0f);

	glPointSize(3.0f);
	glLineWidth(3.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}

int main(int argc, char* argv[]) 
{
	glutInit(&argc, argv);

	// setup a depth buffer, double buffer and rgba mode
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);

	// set the windows initial position and size
	glutInitWindowPosition(50, 50);
	glutInitWindowSize(800, 600);

	// create the window
	glutCreateWindow(TITLE);

	// register the callbacks to glut
	glutDisplayFunc(Render);
	glutReshapeFunc(Resize);
	glutIdleFunc(Update);
	glutKeyboardFunc(OnKeyDown);	
	glutKeyboardUpFunc(OnKeyUp);
	glutSpecialFunc(OnSpecialDown);
	glutSpecialUpFunc(OnSpecialUp);	
	glutJoystickFunc(OnJoystick, 300);	
	
	InitOpenGL();

	scene = std::make_unique<Scene>();
	scene->Initialize();
	scene->OnWindowResize(800, 600);

	// run the program
	glutMainLoop();

	return 0;
}
