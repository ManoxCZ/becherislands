#pragma once

#include <string>


class LocalizedString
{
public:
	std::string Value;

	LocalizedString();
	LocalizedString(const std::string& value);
};

