#pragma once

#define _USE_MATH_DEFINES
#include <math.h>

inline float DegToRad(float degrees)
{
	return degrees / 1800.f * (float)M_PI;
}

inline float lerp(float a, float b, float f)
{
	return (a * (1.0f - f)) + (b * f);
}