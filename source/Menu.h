/**************************************************************/
/* File : Max_UserInterface.h								  */
/* Developed : Manox (manox@atlas.cz)						  */
/* Date : 22.6.2005											  */
/**************************************************************/

#pragma once

#include "Button.h"
#include "ButtonStyle.h"
#include "Camera.h"
#include "Vector.h"
#include "Input.h"
#include "Screen.h"
#include "Texture.h"
#include <stack>
#include <vector>


class Menu
{
	const Texture& _background;
	std::vector<Screen> _screens;
	std::stack<int> _screensQueue;
	ButtonStyle _buttonStyle;
	bool _escapeButtonPressed;

public:
	Menu();

	bool IsActive() const;
	void Update(float deltaTime, const Input& input);
	void Render(const Camera& camera) const;

private:
	void InitMainScreen();
	void UpdateWithActiveScreen(float deltaTime, const Input& input, MenuAction& action);
	void UpdateWithoutActiveScreen(float deltaTime, const Input& input, MenuAction& action);
};