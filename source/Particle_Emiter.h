/**************************************************************/
/* File : Max_Particle_Emiter.h								  */
/* Developed : Manox (manox@atlas.cz)						  */
/* Date : 8.6.2005											  */
/**************************************************************/

#ifndef _PARTICLEEMITER_H
#define _PARTICLEEMITER_H

#include "Particle.h"
#include "Vector.h"
#include "Color.h"
#include <vector>

class ParticleEmitter 
{
private:
	std::vector<Particle> _particles;
	int _actualParticle;
	float _spawningTime;
	Texture _texture;

public:
	Vector3f Position;	
	Vector3f Velocity;
	float SpawnRatio;
	
	ParticleEmitter(float totalLifetime, 
		const Color& startColor, const Color& endColor,
		float startSize, float endSize, 		 
		int maxParticlesCount);
	~ParticleEmitter();

	void Initialize();
	void Update(float deltaTime);
	void SpawnParticles(int count);
	void Render() const;	
	void Reset();
};

#endif

