﻿/**************************************************************/
/* File : Max_UserInterface.cpp								  */
/* Developed : Manox (manox@atlas.cz)						  */
/* Date : 22.6.2005											  */
/**************************************************************/

#include "Menu.h"
#include "RenderPrimitives.h"
#include "Text.h"
#include <windows.h>
#include <gl\gl.h>


Menu::Menu()
	:
	/*new_game(550, 340, Color(0, 0, 0), "Nová hra", "New game", 1, true),
	load_game(550, 370, Color(0, 0, 0), "Nahrát hru", "Load game", 2, true),
	options(550, 400, Color(0, 0, 0), "Nastavení", "Options", 3, true),
	credits(550, 430, Color(0, 0, 0), "Autoøi", "Credits", 4, true),
	exit(550, 460, Color(0, 0, 0), "Konec", "Quit", 5, true),
	language(150, 170, Color(0, 0, 0), "Èesky", "English", 11, false),
	reflex(150, 210, Color(0, 0, 0), "Odrazy na vodì",	"Reflection", 8, false),
	back(450, 500, Color(0, 0, 0), "Zpìt", "Back", 9, false),
	contin(550, 310, Color(0, 0, 0), "Pokraèovat", "Continue", 10, false),
	yes(250, 250, Color(0, 0, 0), "Ano", "Yes", 6, false),
	no(400, 250, Color(0, 0, 0), "Ne", "No", 7, false),
	*/	
	_background(Texture::GetTexture("data\\textures\\hud\\Background.png")),
	_buttonStyle()
{
	InitMainScreen();
}

void Menu::InitMainScreen()
{
	Screen menuScreen;

	menuScreen.Buttons.push_back(Button(_buttonStyle, Vector2f(0.75f, 0.5f), Vector2f(0.2f, 0.1f), "Play"));
	menuScreen.Buttons.push_back(Button(_buttonStyle, Vector2f(0.75f, 0.45f), Vector2f(0.2f, 0.1f), "Load game"));
	menuScreen.Buttons.push_back(Button(_buttonStyle, Vector2f(0.75f, 0.4f), Vector2f(0.2f, 0.1f), "Options"));
	menuScreen.Buttons.push_back(Button(_buttonStyle, Vector2f(0.75f, 0.35f), Vector2f(0.2f, 0.1f), "Credits"));
	menuScreen.Buttons.push_back(Button(_buttonStyle, Vector2f(0.75f, 0.3f), Vector2f(0.2f, 0.1f), "Exit"));

	menuScreen.Buttons.at(0).MenuAction = MenuAction(ActionType::PopScreen);
	menuScreen.Buttons.at(0).IsSelected = true;

	_screens.push_back(std::move(menuScreen));
	_screensQueue.push((int)ScreenId::MainMenu);
}

bool Menu::IsActive() const
{
	return _screensQueue.size() > 0;
}

void Menu::Update(float deltaTime, const Input& input)
{
	MenuAction action;

	if (_screensQueue.size() == 0)
	{
		UpdateWithoutActiveScreen(deltaTime, input, action);
	}
	else
	{
		UpdateWithActiveScreen(deltaTime, input, action);
	}

	switch (action.Type)
	{
	case ActionType::PopScreen:
		_screensQueue.pop();
		break;
	
	default:
		break;
	}



	//glClear(GL_COLOR_BUFFER_BIT); /* vymazani barvoveho bufferu */

	///*if (play && !FSOUND_IsPlaying(0))
	//{
	//	int hudba_vol = rand()%3 + 1;
	//	if (hudba_vol == 1) FSOUND_PlaySound(0, hudba_1);
	//	else if (hudba_vol == 2) FSOUND_PlaySound(0, hudba_2);
	//	else if (hudba_vol == 3) FSOUND_PlaySound(0, hudba_3);
	//}*/

	//if (volba == 15)
	//	kredity.render();
	//else
	//{
	//	//onDisplay(deltaTime);

	//	glDisable(GL_DEPTH_TEST);
	//	glDisable(GL_BLEND);

	//	if (mise.ukoly.size() > 0)
	//	{
	//		if (mise.ukoly[0].molo == 100)
	//			mise.aktualizace(100);
	//		else if (mise.ukoly[0].molo == 120)
	//			mise.aktualizace(120);
	//		else if (mise.ukoly[0].molo == 121)
	//		{
	//			volba = 15;
	//			kredity.init();
	//			hudba_tit.Play();
	//			//FSOUND_StopSound(FSOUND_ALL);
	//			//FSOUND_PlaySound(0, hudba_tit);
	//			mise.ukoly.pop_front();
	//			play = false;
	//		}
	//	}
	//	else if (dialogy.size() == 0 && play)
	//	{
	//		mise.mise = mise.mise + 1;
	//		int newlevel = mise.mise;
	//		std::ofstream saving("data/levels/save.dat");
	//		saving << newlevel << std::endl;
	//		saving << scene->jachta.pozice.X << std::endl;
	//		saving << scene->jachta.pozice.Y << std::endl;
	//		saving << scene->jachta.pozice.Z << std::endl;
	//		saving << scene->jachta.uhel.X << std::endl;
	//		saving << scene->jachta.uhel.Y << std::endl;
	//		saving << scene->jachta.uhel.Z << std::endl;

	//		std::string cestaf;
	//		if (jazyk == 0) cestaf = "data/levels/levels_czech.dat";
	//		else cestaf = "data/levels/levels_eng.dat";

	//		std::ifstream seznam(cestaf);

	//		std::string loading;
	//		for (unsigned int i = 0; i <= mise.mise; i++)
	//			getline(seznam, loading);

	//		char *cesta = new char[loading.size() + 1];
	//		for (unsigned int kk = 0; kk < loading.size(); kk++)
	//			cesta[kk] = loading[kk];
	//		cesta[loading.size()] = '\0';
	//		mise.load_level(cesta);
	//		scene->jachta.reset();
	//		play = true;
	//		//FSOUND_SetPaused(FSOUND_ALL,FALSE);
	//	}

	//	if (dialogy.size() > 0 && play) 
	//		dialogy.front().render();
	//	
	//	if (play && keys[VK_SPACE])
	//	{
	//		if (dialogy.size() > 0)
	//		{
	//			dialogy.pop_front();
	//			keys[VK_SPACE] = false;
	//		}
	//	}

	//	if (((mise.time < 0) || (scene->jachta.zdravi <= 0)) && play)
	//	{
	//		failed = true;
	//		/*FSOUND_StopSound(FSOUND_ALL);
	//		FSOUND_PlaySound(0, samp1);*/
	//		play = false;
	//		volba = 0;
	//		new_game.aktive = true;
	//		load_game.aktive = true;
	//		options.aktive = true;
	//		credits.aktive = true;
	//		exit.aktive = true;
	//		contin.aktive = true;
	//		esc_key = false;
	//		contine = true;
	//		buff_position = scene->jachta.pozice;
	//		buff_angle = scene->jachta.uhel;
	//		scene->jachta.pozice.X = 4270;
	//		scene->jachta.pozice.Z = 1080;
	//		scene->jachta.pozice.Y = 1;
	//		scene->jachta.uhel.Y = 200;
	//	}

	//	if (failed)
	//	{
	//		if (jazyk == 0) printText("Mise nesplnìna", font_24, 300, 300, 1, 0, 0);
	//		else printText("Mission failed", font_24, 300, 300, 1, 0, 0);
	//	}

	//	if (!play)
	//	{

	//		GetCursorPos(&mouse_pos);

	//		glDisable(GL_DEPTH_TEST);
	//		glDisable(GL_BLEND);
	//		glEnable(GL_TEXTURE_2D);
	//		
	//		pozadi.Bind();
	//		
	//		glBegin(GL_QUADS);              /* zacatek zadavani ctverce */
	//		glTexCoord2f(0.0, 1.0);
	//		glVertex2i(0, 0);			/* poloha prvniho vertexu */
	//		glTexCoord2f(1.0, 1.0);
	//		glVertex2i(1024, 0);		/* poloha druheho vertexu */
	//		glTexCoord2f(1.0, 0.0);
	//		glVertex2i(1024, 1024);/* poloha tretiho vertexu */
	//		glTexCoord2f(0.0, 0.0);
	//		glVertex2i(0, 1024);		/* poloha ctvrteho vertexu */
	//		glEnd();

	//		stream = 0;
	//		if (volba == 0)
	//		{
	//			if (contine && !failed) stream = contin.render(mouse_pos.x, mouse_pos.y, stream);
	//			stream = new_game.render(mouse_pos.x, mouse_pos.y, stream);
	//			stream = load_game.render(mouse_pos.x, mouse_pos.y, stream);
	//			stream = options.render(mouse_pos.x, mouse_pos.y, stream);
	//			stream = credits.render(mouse_pos.x, mouse_pos.y, stream);
	//			stream = exit.render(mouse_pos.x, mouse_pos.y, stream);
	//			volba = stream;
	//			if (volba == 5)
	//			{
	//				//FSOUND_PlaySound(1, sound_tlac);
	//				new_game.aktive = false;
	//				load_game.aktive = false;
	//				options.aktive = false;
	//				credits.aktive = false;
	//				exit.aktive = false;

	//				yes.aktive = true;
	//				no.aktive = true;
	//			}
	//			if (volba == 1)
	//			{
	//				//FSOUND_PlaySound(1, sound_tlac);
	//				new_game.aktive = false;
	//				load_game.aktive = false;
	//				options.aktive = false;
	//				credits.aktive = false;
	//				exit.aktive = false;

	//				yes.aktive = true;
	//				no.aktive = true;
	//			}
	//			else if (volba == 2)
	//			{
	//				failed = false;
	//				//FSOUND_PlaySound(1, sound_tlac);
	//				std::string loading;
	//				std::ifstream savefile("data/levels/save.dat");

	//				getline(savefile, loading);
	//				int level = atoi(loading.c_str());
	//				if (level != 0)
	//				{
	//					mise.mise = level;

	//					float poloha;
	//					getline(savefile, loading);
	//					poloha = atof(loading.c_str());
	//					scene->jachta.pozice.X = poloha;

	//					getline(savefile, loading);
	//					poloha = atof(loading.c_str());
	//					scene->jachta.pozice.Y = poloha;

	//					getline(savefile, loading);
	//					poloha = atof(loading.c_str());
	//					scene->jachta.pozice.Z = poloha;

	//					getline(savefile, loading);
	//					poloha = atof(loading.c_str());
	//					scene->jachta.uhel.X = poloha;

	//					getline(savefile, loading);
	//					poloha = atof(loading.c_str());
	//					scene->jachta.uhel.Y = poloha;

	//					getline(savefile, loading);
	//					poloha = atof(loading.c_str());
	//					scene->jachta.uhel.Z = poloha;

	//					getline(savefile, loading);
	//					poloha = atof(loading.c_str());

	//					savefile.close();
	//					std::ifstream seznam("data/levels/levels_czech.dat");

	//					for (int i = 0; i <= level; i++)
	//						getline(seznam, loading);

	//					char *cesta = new char[loading.size() + 1];
	//					for (unsigned int kk = 0; kk < loading.size(); kk++)
	//						cesta[kk] = loading[kk];
	//					cesta[loading.size()] = '\0';
	//					mise.load_level(cesta);
	//					scene->jachta.reset();
	//					/*FSOUND_SetPaused(FSOUND_ALL,FALSE);
	//					FSOUND_StopSound(0);
	//					int hudba_vol = rand()%3 + 1;
	//					if (hudba_vol == 1) FSOUND_PlaySound(0, hudba_1);
	//					else if (hudba_vol == 2) FSOUND_PlaySound(0, hudba_2);
	//					else if (hudba_vol == 3) FSOUND_PlaySound(0, hudba_3);
	//					play = true;*/
	//				}
	//				else volba = 0;
	//			}
	//			else if (volba == 3)
	//			{
	//				sound_tlac.Play();
	//				//FSOUND_PlaySound(1, sound_tlac);
	//				new_game.aktive = false;
	//				load_game.aktive = false;
	//				options.aktive = false;
	//				credits.aktive = false;
	//				exit.aktive = false;

	//				language.aktive = true;
	//				reflex.aktive = true;
	//				back.aktive = true;
	//			}
	//			else if (volba == 4)
	//			{
	//				sound_tlac.Play();
	//				//FSOUND_PlaySound(1, sound_tlac);
	//				volba = 15;
	//				kredity.init();
	//				//FSOUND_StopSound(0);
	//				hudba_tit.Play();
	//				//FSOUND_PlaySound(0, hudba_tit);
	//			}
	//			else if (volba == 10)
	//			{
	//				scene->jachta.pozice = buff_position;
	//				scene->jachta.uhel = buff_angle;
	//				/*FSOUND_SetPaused(FSOUND_ALL,FALSE);
	//				FSOUND_PlaySound(5, sound_tlac);
	//				FSOUND_StopSound(0);*/
	//				sound_tlac.Play();
	//				/*int hudba_vol = rand()%3 + 1;
	//				if (hudba_vol == 1) FSOUND_PlaySound(0, hudba_1);
	//				else if (hudba_vol == 2) FSOUND_PlaySound(0, hudba_2);
	//				else if (hudba_vol == 3) FSOUND_PlaySound(0, hudba_3);*/
	//				play = true;
	//			}
	//		}
	//		else if (volba == 1)
	//		{
	//			std::string loading;
	//			std::ifstream savefile("data/levels/save.dat");

	//			getline(savefile, loading);
	//			int level = atoi(loading.c_str());
	//			if (level != 0) {

	//				if (contine && !failed) contin.render(mouse_pos.x, mouse_pos.y, stream);
	//				new_game.render(mouse_pos.x, mouse_pos.y, stream);
	//				load_game.render(mouse_pos.x, mouse_pos.y, stream);
	//				options.render(mouse_pos.x, mouse_pos.y, stream);
	//				credits.render(mouse_pos.x, mouse_pos.y, stream);
	//				exit.render(mouse_pos.x, mouse_pos.y, stream);
	//				if (jazyk == 0)
	//				{
	//					printText("Pokud zaènete hrát od zaèátku,", font_24, 150, 180, 0, 0, 0);
	//					printText("pøijdete o uloženou pozici !", font_24, 150, 205, 0, 0, 0);
	//				}
	//				else
	//				{
	//					printText("Save game will lost,", font_24, 170, 180, 0, 0, 0);
	//					printText("if you start play new game !", font_24, 170, 205, 0, 0, 0);
	//				}
	//				stream = yes.render(mouse_pos.x, mouse_pos.y, stream);
	//				stream = no.render(mouse_pos.x, mouse_pos.y, stream);
	//				if (stream == 6)
	//				{
	//					sound_tlac.Play();
	//					//FSOUND_PlaySound(1, sound_tlac);
	//					scene->jachta.pozice.X = 6420;
	//					scene->jachta.pozice.Y = 1.5;
	//					scene->jachta.pozice.Z = 3272;
	//					scene->jachta.uhel.Y = 90;
	//					scene->jachta.reset();						
	//					if (jazyk == 0) mise.load_level("data/levels/czech/tutorial.dat");
	//					else mise.load_level("data/levels/english/tutorial.dat");
	//					mise.mise = 0;
	//					scene->jachta.reset();
	//					failed = false;
	//					scene->jachta.zdravi = 100;
	//					play = true;
	//					//FSOUND_StopSound(0);
	//					/*int hudba_vol = rand()%3 + 1;
	//					if (hudba_vol == 1) FSOUND_PlaySound(0, hudba_1);
	//					else if (hudba_vol == 2) FSOUND_PlaySound(0, hudba_2);
	//					else if (hudba_vol == 3) FSOUND_PlaySound(0, hudba_3);*/
	//				}
	//				else if (stream == 7)
	//				{
	//					sound_tlac.Play();
	//					//FSOUND_PlaySound(1, sound_tlac);
	//					new_game.aktive = true;
	//					load_game.aktive = true;
	//					options.aktive = true;
	//					credits.aktive = true;
	//					exit.aktive = true;

	//					yes.aktive = false;
	//					no.aktive = false;
	//					volba = 0;
	//				}
	//			}
	//			else
	//			{
	//				scene->jachta.pozice.X = 6420;
	//				scene->jachta.pozice.Y = 1.5;
	//				scene->jachta.pozice.Z = 3272;
	//				scene->jachta.uhel.Y = 90;
	//				scene->jachta.reset();					
	//				if (jazyk == 0) mise.load_level("data/levels/czech/tutorial.dat");
	//				else mise.load_level("data/levels/english/tutorial.dat");
	//				mise.mise = 0;
	//				scene->jachta.reset();
	//				failed = false;
	//				scene->jachta.zdravi = 100;
	//				play = true;
	//				/*FSOUND_StopSound(0);
	//				int hudba_vol = rand()%3 + 1;
	//				if (hudba_vol == 1) FSOUND_PlaySound(0, hudba_1);
	//				else if (hudba_vol == 2) FSOUND_PlaySound(0, hudba_2);
	//				else if (hudba_vol == 3) FSOUND_PlaySound(0, hudba_3);*/
	//			}
	//		}
	//		else if (volba == 3)
	//		{
	//			if (contine && !failed) contin.render(mouse_pos.x, mouse_pos.y, stream);
	//			new_game.render(mouse_pos.x, mouse_pos.y, stream);
	//			load_game.render(mouse_pos.x, mouse_pos.y, stream);
	//			options.render(mouse_pos.x, mouse_pos.y, stream);
	//			credits.render(mouse_pos.x, mouse_pos.y, stream);
	//			exit.render(mouse_pos.x, mouse_pos.y, stream);

	//			stream = language.render(mouse_pos.x, mouse_pos.y, stream);
	//			stream = reflex.render(mouse_pos.x, mouse_pos.y, stream);
	//			stream = back.render(mouse_pos.x, mouse_pos.y, stream);
	//			if (stream == 8)
	//			{
	//				sound_tlac.Play();
	//				//FSOUND_PlaySound(1, sound_tlac);
	//				refrakce = !refrakce;
	//			}
	//			else if (stream == 11)
	//			{
	//				sound_tlac.Play();
	//				//FSOUND_PlaySound(1, sound_tlac);
	//				if (jazyk == 0) jazyk = 1;
	//				else jazyk = 0;
	//			}
	//			else if (stream == 9)
	//			{
	//				sound_tlac.Play();
	//				//FSOUND_PlaySound(1, sound_tlac);
	//				new_game.aktive = true;
	//				load_game.aktive = true;
	//				options.aktive = true;
	//				credits.aktive = true;
	//				exit.aktive = true;

	//				reflex.aktive = false;
	//				back.aktive = false;
	//				volba = 0;
	//			}
	//		}
	//		else if (volba == 5)
	//		{
	//			if (contine && !failed) contin.render(mouse_pos.x, mouse_pos.y, stream);
	//			new_game.render(mouse_pos.x, mouse_pos.y, stream);
	//			load_game.render(mouse_pos.x, mouse_pos.y, stream);
	//			options.render(mouse_pos.x, mouse_pos.y, stream);
	//			credits.render(mouse_pos.x, mouse_pos.y, stream);
	//			exit.render(mouse_pos.x, mouse_pos.y, stream);
	//			if (jazyk == 0) printText("Opravdu konec ?", font_24, 250, 200, 0, 0, 0);
	//			else printText("Really quit ?", font_24, 250, 200, 0, 0, 0);
	//			stream = yes.render(mouse_pos.x, mouse_pos.y, stream);
	//			stream = no.render(mouse_pos.x, mouse_pos.y, stream);
	//			if (stream == 6)
	//			{
	//				sound_tlac.Play();
	//				//FSOUND_PlaySound(1, sound_tlac);
	//				PostQuitMessage(0);
	//			}
	//			else if (stream == 7)
	//			{
	//				sound_tlac.Play();
	//				//FSOUND_PlaySound(1, sound_tlac);
	//				new_game.aktive = true;
	//				load_game.aktive = true;
	//				options.aktive = true;
	//				credits.aktive = true;
	//				exit.aktive = true;

	//				yes.aktive = false;
	//				no.aktive = false;
	//				volba = 0;
	//			}


	//		}

	//		cursor.Bind();

	//		glBegin(GL_QUADS);              /* zacatek zadavani ctverce */
	//		glTexCoord2f(0.0, 1.0);
	//		glVertex2i(mouse_pos.x, mouse_pos.y);			/* poloha prvniho vertexu */
	//		glTexCoord2f(1.0, 1.0);
	//		glVertex2i(mouse_pos.x + 32, mouse_pos.y);		/* poloha druheho vertexu */
	//		glTexCoord2f(1.0, 0.0);
	//		glVertex2i(mouse_pos.x + 32, mouse_pos.y + 32);/* poloha tretiho vertexu */
	//		glTexCoord2f(0.0, 0.0);
	//		glVertex2i(mouse_pos.x, mouse_pos.y + 32);		/* poloha ctvrteho vertexu */
	//		glEnd();

	//		stiskL = false;
	//	}
	//	else
	//	{
	//		if (esc_key)
	//		{
	//			samp1.Play();
	//			/*FSOUND_SetPaused(FSOUND_ALL,TRUE);
	//			FSOUND_PlaySound(0, samp1);*/
	//			play = false;
	//			volba = 0;
	//			new_game.aktive = true;
	//			load_game.aktive = true;
	//			options.aktive = true;
	//			credits.aktive = true;
	//			exit.aktive = true;
	//			contin.aktive = true;
	//			esc_key = false;
	//			contine = true;
	//			buff_position = scene->jachta.pozice;
	//			buff_angle = scene->jachta.uhel;
	//			scene->jachta.pozice.X = 4270;
	//			scene->jachta.pozice.Z = 1080;
	//			scene->jachta.pozice.Y = 1;
	//			scene->jachta.uhel.Y = 200;
	//		}
	//	}
	//}
}

void Menu::UpdateWithActiveScreen(float deltaTime, const Input& input, MenuAction& action)
{
	action = _screens.at(_screensQueue.top()).Update(deltaTime, input);
}

void Menu::UpdateWithoutActiveScreen(float deltaTime, const Input& input, MenuAction& action)
{
	bool escapeButtonPressed = input.IsKeyPressed((unsigned char)27);
	
	if (_escapeButtonPressed && !escapeButtonPressed)
	{
		_screensQueue.push((int)ScreenId::MainMenu);
	}

	_escapeButtonPressed = escapeButtonPressed;
}

void Menu::Render(const Camera& camera) const
{
	glMatrixMode(GL_PROJECTION);

	glPushMatrix();

	glLoadIdentity();
	glOrtho(0, 1, 0, 1, -1, 1);

	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GEQUAL, 1);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	_background.Bind();

	RenderPrimitives::RenderQuad2D(Vector2f(0, 0), Vector2f(1, 1));

	_screens.at(_screensQueue.top()).Render(camera);

	glMatrixMode(GL_PROJECTION);

	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
}





