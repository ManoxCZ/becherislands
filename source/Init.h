/**************************************************************/
/* File : Max_Init.h										  */
/* Developed : Manox (manox@atlas.cz)						  */
/* Date : 16.5.2005											  */
/**************************************************************/

#ifndef _INIT_H
#define _INIT_H

#include "Terrain.h"

bool Init(void);
void GenerateObjectsOnTerrain(Terrain& terrain);

#endif
