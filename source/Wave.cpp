/**************************************************************/
/* File : Max_Wave.cpp										  */
/* Developed : Manox (manox@atlas.cz)						  */
/* Date : 1.8.2005											  */
/**************************************************************/

#include "Wave.h"
#include "Texture.h"
#include <windows.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include <gl\glut.h>

SeaWave::SeaWave(Vector3f poc1, Vector3f poc2, Vector3f kon1, Vector3f kon2)
	:
	pocatek1(poc1),
	pocatek2(poc2),
	konec1(kon1),
	konec2(kon2)
{
	textura = Texture::GetTexture("data\\textures\\wave.bmp");
	korekce = 0.1;

	zarazka1 = pocatek1*2 - konec1;
	zarazka2 = pocatek2*2 - konec2;
	
	//pocatek1.y = 0.1;
	//pocatek2.y = 0.1;
	//konec1.y = 0.1;
	//konec2.y = 0.1;
	zarazka1.Y = 0.1;
	zarazka2.Y = 0.1;
}

void SeaWave::Update(float deltaTime)
{
	roh1 = zarazka1;// + (pocatek1-zarazka1)*t; 
	roh2 = zarazka2;// + (pocatek2-zarazka2)*t;
	roh3 = pocatek1 + (konec1 - pocatek1) * t;
	roh4 = pocatek2 + (konec2 - pocatek2) * t;
	roh5 = zarazka1;// + (pocatek1-zarazka1)*(1-t); 
	roh6 = zarazka2;// + (pocatek2-zarazka2)*(1-t);
	roh7 = pocatek1 + (konec1 - pocatek1) * (1 - t);
	roh8 = pocatek2 + (konec2 - pocatek2) * (1 - t);
	if (direction) if (t > 0.5) t += deltaTime * (1 - t + 0.1) * 2;
	else t += deltaTime * (t + 0.1) * 2;
	else if (t > 0.5) t -= deltaTime * (1 - t + 0.1) * 2;
	else t -= deltaTime * (t + 0.1) * 2;
	if (t >= 1.0)
	{
		direction = false;
	}
	if (t <= 0.0)
	{
		direction = true;
	}
}

void SeaWave::Render() const
{
	textura.Bind();
	
	glBegin(GL_TRIANGLES);
		glTexCoord2f(0.0f,1.0f);
		glVertex3f(roh1.X,roh1.Y+korekce,roh1.Z);
		glTexCoord2f(2.0f,1.0f);
		glVertex3f(roh2.X,roh2.Y+korekce,roh2.Z);
		glTexCoord2f(2.0f,0.0f);
		glVertex3f(roh4.X,roh4.Y+korekce,roh4.Z);
		glTexCoord2f(0.0f,1.0f);
		glVertex3f(roh1.X,roh1.Y+korekce,roh1.Z);
		glTexCoord2f(2.0f,0.0f);
		glVertex3f(roh4.X,roh4.Y+korekce,roh4.Z);
		glTexCoord2f(0.0f,0.0f);
		glVertex3f(roh3.X,roh3.Y+korekce,roh3.Z);
	
		glTexCoord2f(0.0f,1.0f);
		glVertex3f(roh5.X,roh5.Y+korekce,roh5.Z);
		glTexCoord2f(5.0f,1.0f);
		glVertex3f(roh6.X,roh6.Y+korekce,roh6.Z);
		glTexCoord2f(5.0f,0.0f);
		glVertex3f(roh8.X,roh8.Y+korekce,roh8.Z);
		glTexCoord2f(0.0f,1.0f);
		glVertex3f(roh5.X,roh5.Y+korekce,roh5.Z);
		glTexCoord2f(5.0f,0.0f);
		glVertex3f(roh8.X,roh8.Y+korekce,roh8.Z);
		glTexCoord2f(0.0f,0.0f);
		glVertex3f(roh7.X,roh7.Y+korekce,roh7.Z);
	glEnd();
}
