#include "Text.h"

#include <gl\glut.h>
#include <gl\freeglut.h>

void RenderText(const std::string& text, const Vector2f& position, const Color& color)
{
	glDisable(GL_ALPHA_TEST);
	glDisable(GL_LIGHTING);
	glBindTexture(GL_TEXTURE_2D, 0);

	glColor4f(color.R, color.G, color.B, color.A);
	glRasterPos2f(position.X, position.Y);
			
	glutBitmapString(GLUT_BITMAP_HELVETICA_18, (const unsigned char*)text.c_str());
	
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
}