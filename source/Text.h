#pragma once

#include "Color.h"
#include "Vector.h"
#include <string>


void RenderText(const std::string& text, const Vector2f& position, const Color& color);