#pragma once

#include "ButtonStyle.h"
#include "Color.h"
#include "MenuAction.h"
#include "Vector.h"
#include "Texture.h"
#include <string>

class Button
{
public:
	const ButtonStyle& Style;
	MenuAction MenuAction;
	Vector2f Position;
	Vector2f Size;		
	bool IsSelected;
	std::string Text;	
	
	Button(const ButtonStyle& style, const Vector2f& position, const Vector2f& size, const std::string& text);
	
	void Render() const;
};