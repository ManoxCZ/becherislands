/**************************************************************/
/* File : Max_Skybox.cpp									  */
/* Developed : Manox (manox@atlas.cz)						  */
/* Date : 8.6.2005											  */
/**************************************************************/

#include "Skybox.h"
#include <Windows.h>
#include <gl\gl.h>
#include <gl\glu.h>

Skybox::Skybox()
	:
	_top(Texture::GetTexture("data/textures/skybox/top.bmp")),
	_back(Texture::GetTexture("data/textures/skybox/back.bmp")),
	_front(Texture::GetTexture("data/textures/skybox/front.bmp")),
	_right(Texture::GetTexture("data/textures/skybox/right.bmp")),
	_left(Texture::GetTexture("data/textures/skybox/left.bmp")),
	_bottom(Texture::GetTexture("data/textures/skybox/dno.bmp")),
	_sky(Texture::GetTexture("data/textures/skybox/sky.bmp"))
{
}

void Skybox::Render(const Camera& camera) const
{
	glEnable(GL_TEXTURE_2D);
	glDisable(GL_LIGHT0);	
	glDisable(GL_DEPTH_TEST);

	glLoadIdentity();
	
	camera.SetLookAtRotationOnly();
	
	_top.Bind();

	glBegin(GL_QUADS);
	glTexCoord2f(0.999f, 0.999);
	glVertex3f(-SIZE, HEIGHT, SIZE);
	glTexCoord2f(0.999, 0.001);
	glVertex3f(SIZE, HEIGHT, SIZE);
	glTexCoord2f(0.001, 0.001);
	glVertex3f(SIZE, HEIGHT, -SIZE);
	glTexCoord2f(0.001, 0.999);
	glVertex3f(-SIZE, HEIGHT, -SIZE);

	glTexCoord2f(0.999, 0.999);
	glVertex3f(-SIZE, -HEIGHT, SIZE);
	glTexCoord2f(0.001, 0.999);
	glVertex3f(-SIZE, -HEIGHT, -SIZE);
	glTexCoord2f(0.001, 0.001);
	glVertex3f(SIZE, -HEIGHT, -SIZE);
	glTexCoord2f(0.999, 0.001);
	glVertex3f(SIZE, -HEIGHT, SIZE);
	glEnd();

	_back.Bind();

	glBegin(GL_QUADS);
	glTexCoord2f(0.999, 0.999);
	glVertex3f(-SIZE, HEIGHT, SIZE);
	glTexCoord2f(0.001, 0.999);
	glVertex3f(SIZE, HEIGHT, SIZE);
	glTexCoord2f(0.001, 0.001);
	glVertex3f(SIZE, -HEIGHT, SIZE);
	glTexCoord2f(0.999, 0.001);
	glVertex3f(-SIZE, -HEIGHT, SIZE);
	glEnd();

	_front.Bind();

	glBegin(GL_QUADS);
	glTexCoord2f(0.001, 0.999);
	glVertex3f(-SIZE, HEIGHT, -SIZE);
	glTexCoord2f(0.999, 0.999);
	glVertex3f(SIZE, HEIGHT, -SIZE);
	glTexCoord2f(0.999, 0.001);
	glVertex3f(SIZE, -HEIGHT, -SIZE);
	glTexCoord2f(0.001, 0.001);
	glVertex3f(-SIZE, -HEIGHT, -SIZE);
	glEnd();

	_right.Bind();

	glBegin(GL_QUADS);
	glTexCoord2f(0.001, 0.999);
	glVertex3f(-SIZE, HEIGHT, SIZE);
	glTexCoord2f(0.999, 0.999);
	glVertex3f(-SIZE, HEIGHT, -SIZE);
	glTexCoord2f(0.999, 0.001);
	glVertex3f(-SIZE, -HEIGHT, -SIZE);
	glTexCoord2f(0.001, 0.001);
	glVertex3f(-SIZE, -HEIGHT, SIZE);
	glEnd();

	_left.Bind();

	glBegin(GL_QUADS);
	glTexCoord2f(0.999, 0.999);
	glVertex3f(SIZE, HEIGHT, SIZE);
	glTexCoord2f(0.001, 0.999);
	glVertex3f(SIZE, HEIGHT, -SIZE);
	glTexCoord2f(0.001, 0.001);
	glVertex3f(SIZE, -HEIGHT, -SIZE);
	glTexCoord2f(0.999, 0.001);
	glVertex3f(SIZE, -HEIGHT, SIZE);
	glEnd();

	_bottom.Bind();

	glBegin(GL_QUADS);
	glTexCoord2f(0.999, 0.999);
	glVertex3f(-SIZE, 0, SIZE);
	glTexCoord2f(0.999, 0.001);
	glVertex3f(SIZE, 0, SIZE);
	glTexCoord2f(0.001, 0.001);
	glVertex3f(SIZE, 0, -SIZE);
	glTexCoord2f(0.001, 0.999);
	glVertex3f(-SIZE, 0, -SIZE);
	glEnd();

	glEnable(GL_DEPTH_TEST);	
	glEnable(GL_LIGHT0);
}

void Skybox::RenderWaterReflection()
{
	glEnable(GL_BLEND);
	glDisable(GL_CULL_FACE);

	glBlendFunc(GL_SRC_COLOR, GL_DST_COLOR); //Typ blendingu

	_sky.Bind();

	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-SIZE, -0.25, SIZE);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(SIZE, -0.25, SIZE);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(SIZE, -0.25, -SIZE);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-SIZE, -0.25, -SIZE);
	glEnd();

	/*
		glBindTexture(GL_TEXTURE_2D, textury[pocatek_textur+1]);
		glBegin(GL_QUADS);
			glTexCoord2f(0.999,0.999);
			glVertex3f(-velikost_oblohy,vyska_oblohy,velikost_oblohy);
			glTexCoord2f(0.001,0.999);
			glVertex3f(velikost_oblohy,vyska_oblohy,velikost_oblohy);
			glTexCoord2f(0.001,0.001);
			glVertex3f(velikost_oblohy,-vyska_oblohy,velikost_oblohy);
			glTexCoord2f(0.999,0.001);
			glVertex3f(-velikost_oblohy,-vyska_oblohy,velikost_oblohy);
		glEnd();

		glBindTexture(GL_TEXTURE_2D, textury[pocatek_textur+2]);
		glBegin(GL_QUADS);
			glTexCoord2f(0.001,0.999);
			glVertex3f(-velikost_oblohy,vyska_oblohy,-velikost_oblohy);
			glTexCoord2f(0.999,0.999);
			glVertex3f(velikost_oblohy,vyska_oblohy,-velikost_oblohy);
			glTexCoord2f(0.999,0.001);
			glVertex3f(velikost_oblohy,-vyska_oblohy,-velikost_oblohy);
			glTexCoord2f(0.001,0.001);
			glVertex3f(-velikost_oblohy,-vyska_oblohy,-velikost_oblohy);
		glEnd();

		glBindTexture(GL_TEXTURE_2D, textury[pocatek_textur+3]);
		glBegin(GL_QUADS);
			glTexCoord2f(0.001,0.999);
			glVertex3f(-velikost_oblohy,vyska_oblohy,velikost_oblohy);
			glTexCoord2f(0.999,0.999);
			glVertex3f(-velikost_oblohy,vyska_oblohy,-velikost_oblohy);
			glTexCoord2f(0.999,0.001);
			glVertex3f(-velikost_oblohy,-vyska_oblohy,-velikost_oblohy);
			glTexCoord2f(0.001,0.001);
			glVertex3f(-velikost_oblohy,-vyska_oblohy,velikost_oblohy);
		glEnd();

		glBindTexture(GL_TEXTURE_2D, textury[pocatek_textur+4]);
		glBegin(GL_QUADS);
			glTexCoord2f(0.999,0.999);
			glVertex3f(velikost_oblohy,vyska_oblohy,velikost_oblohy);
			glTexCoord2f(0.001,0.999);
			glVertex3f(velikost_oblohy,vyska_oblohy,-velikost_oblohy);
			glTexCoord2f(0.001,0.001);
			glVertex3f(velikost_oblohy,-vyska_oblohy,-velikost_oblohy);
			glTexCoord2f(0.999,0.001);
			glVertex3f(velikost_oblohy,-vyska_oblohy,velikost_oblohy);
		glEnd();
	*/
}

