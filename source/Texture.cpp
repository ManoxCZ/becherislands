/**************************************************************/
/* File : Max_Texture.cpp									  */
/* Developed : Manox (manox@atlas.cz)						  */
/* Date : 25.5.2005											  */
/**************************************************************/

#include "Texture.h"
#include <windows.h>
#include <gl\gl.h>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

unsigned int Texture::LoadTexture(const std::string& file)
{
	int width, height, numChannels;
	
	stbi_set_flip_vertically_on_load(true);

	unsigned char *data = stbi_load(file.c_str(), &width, &height, &numChannels, 0);	

	if (_freeTextureHandles.size() == 0)
	{
		std::vector<unsigned int> textureHadles;
		textureHadles.resize(100);

		glGenTextures(100, textureHadles.data());		

		for (const unsigned int& id: textureHadles)
		{
			_freeTextureHandles.push(id);
		}
	}

	unsigned int handle = _freeTextureHandles.front();
	_freeTextureHandles.pop();

	glBindTexture(GL_TEXTURE_2D, handle);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	int format = (numChannels == 3) ? GL_RGB : GL_RGBA;

	glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		
	stbi_image_free(data);

	return handle;
}

Texture::Texture()
	: _handle(0)
{

}

Texture::Texture(unsigned int handle)
	: _handle(handle)
{

}

Texture::Texture(const Texture& other)
	: _handle(other._handle)
{

}

const Texture& Texture::GetTexture(const std::string& filename)
{
	if (_textures.size() == 0 || _textures.find(filename) == _textures.end())
	{
		unsigned int handle = Texture::LoadTexture(filename);

		_textures.insert({ filename, Texture(handle) });
	}

	return _textures[filename];
}

void Texture::Bind() const
{
	glBindTexture(GL_TEXTURE_2D, _handle);
}