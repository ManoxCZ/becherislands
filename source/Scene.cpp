/**************************************************************/
/* File : Max_Render.cpp									  */
/* Developed : Manox (manox@atlas.cz)						  */
/* Date : 16.5.2005											  */
/**************************************************************/

#include "Init.h"
#include "Scene.h"
#include <windows.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include <gl\glut.h>


//int mouse_x, mouse_y;
//
//POINT mouse_pos;
//
//GLfloat scenerotz;
//float pozice_x;
//float pozice_y=100;
//float pozice_z;
//float rot_x;
//float rot_y;
//float rot_z;
//float pzice_x;
//float pzice_y;
//float pzice_z;
//float wave_movement;


float lt_position[]={2500.0, 5000.0, 2500.0, 1.0};   

float posun;
bool dir;

void Scene::onDisplay(const Camera& camera, float deltaTime)
{
	glClearColor(1, 0, 0, 1);
	glClearDepth(1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	_skybox.Render(camera);

	terrain.Render(camera);	

	//terrain.Update(deltaTime);

	//pozice_x = jachta.pozice.X;
	//pozice_y = jachta.pozice.Y;
	//pozice_z = jachta.pozice.Z;
	//rot_y = jachta.uhel.Y;
	//rot_z = jachta.uhel.Z;
	//hloubka = terrain.height(jachta.pozice.X, jachta.pozice.Z);
 //  
	//pozice_x += + sin(0.0174532925f*jachta.uhel.Y)*50;
	//pozice_y += 20;
	//pozice_z += + cos(0.0174532925f*jachta.uhel.Y)*50;

 //   scenerotx = 360.0f - rot_y;
 //   scenerotz = 360.0f - rot_z;

	//// konec sprava mouse
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT); /* vymazani barvoveho bufferu */
 //   glMatrixMode(GL_MODELVIEW);                   /* bude se menit modelova matice */
 //   glLoadIdentity();  

	//glMatrixMode(GL_PROJECTION);                  /* bude se menit projekcni matice */
 //   glLoadIdentity();                             /* nastaveni jednotkove matice */
 //   gluPerspective(fov, (double)800/(double)600, near_plane, far_plane);

	//glRotatef(scenerotz,1.0f,0,0);
 //   glRotatef(scenerotx,0,1.0f,0);

	//glTranslatef(-pozice_x, -pozice_y, -pozice_z);

	//glEnable(GL_LIGHT0);

	//glBlendFunc(GL_ONE_MINUS_SRC_COLOR, GL_SRC_COLOR); //Typ blendingu 

	//glLightfv(GL_LIGHT0,GL_POSITION,lt_position);
	//glDisable(GL_LIGHTING);

	//_skybox.renderSkybox();
	
//	// odraz 
//	if (refrakce)
//	{
//		glEnable(GL_BLEND);
//		glDisable(GL_DEPTH_TEST);
//
//		terrain.render_ref();
//
//		glEnable(GL_DEPTH_TEST);
//
//		glDisable(GL_BLEND);
//
//		glScalef(1,-1,1);
//		glEnable(GL_CLIP_PLANE0);
//		glEnable(GL_CULL_FACE);                          /* zapnuti maskovani polygonu */
//
//		/*for (unsigned int i=0; i< terrain.viditelne.size(); i++)
//		{
//			glDisable(GL_CULL_FACE);
//
//			if (terrain.viditelne[i].vzdalenost < 100)
//			{
//				for (const ModelInstance& instance: Objects[terrain.viditelne[i].ktery])
//				{
//					instance.Render();
//				}
//			}
//			else
//			{
//				for (const ModelInstance& instance : Objects[terrain.viditelne[i].ktery])
//				{
//					instance.RenderLOD();
//				}
//			}
//			
//			glEnable(GL_CULL_FACE);					
//		}	*/
//		glDisable(GL_CULL_FACE);                          /* zapnuti maskovani polygonu */
//			
//		if (play) jachta.Render();	
//		else jachta_menu.Render();
//		glDisable(GL_LIGHTING);
//
//		glDisable(GL_CLIP_PLANE0);
//		GLdouble eqn1[] = {0.0,1.0,0.0,0.0}; 
//		glClipPlane(GL_CLIP_PLANE0, eqn1);
//		glScalef(1,-1,1);
//
//		glEnable(GL_DEPTH_TEST);
//		glDisable(GL_BLEND);
//	}
//	// konec odrazu
//
//	glEnable(GL_DEPTH);
//	glEnable(GL_DEPTH_TEST);
//
//	if (refrakce) glEnable(GL_CLIP_PLANE0);	
//	terrain.render();
//	//if (refrakce) glDisable(GL_CLIP_PLANE0);
//	glDisable(GL_CLIP_PLANE0);
//	glEnable(GL_LIGHTING);
//	                     
//	/*for (unsigned int i=0; i< terrain.viditelne.size(); i++)
//	{
//		glDisable(GL_CULL_FACE); 
//		glDisable(GL_LIGHTING);
//
//		if (terrain.viditelne[i].vzdalenost < 100)
//		{
//			for (const ModelInstance& instance : Objects[terrain.viditelne[i].ktery])
//			{
//				instance.Render();
//			}
//		}
//		else
//		{
//			for (const ModelInstance& instance : Objects[terrain.viditelne[i].ktery])
//			{
//				instance.RenderLOD();
//			}
//		}
//	}	*/
//	if (refrakce) glDisable(GL_CLIP_PLANE0);
//
//	//glDisable(GL_CULL_FACE);                          /* zapnuti maskovani polygonu */
//	//if (play) jachta.Render();	
//	//else jachta_menu.Render();
//
//	//glDisable(GL_LIGHTING);
//
//	//terrain.render_water();
//	//glDisable(GL_BLEND);
//	//	
//	//for (unsigned xxx = 0; xxx < vlny.size(); xxx++)
//	//	vlny[xxx].render_Wave();
//
//	
//	
//
//// zacatek 2D vykreslovani
//
//	//glMatrixMode(GL_PROJECTION);            // zacatek modifikace projekcni matice 
//	//glLoadIdentity(); 
// //   glOrtho(0, 800, 0, 600, -1, 1);        // mapovani abstraktnich souradnic do souradnic okna 
// //   glScalef(1, -1, 1);                     // inverze y-ove osy, aby se y zvetsovalo smerem dolu 
// //   glTranslatef(0, -600, 0);               // posun pocatku do leveho horniho rohu 
//	//glDisable(GL_LIGHTING);
//	//glDisable(GL_CULL_FACE);
//
//	//
//	//
//	//glPushMatrix();
//	//if (play) HUD.renderInterface();
//	//glPopMatrix();

}

Scene::Scene()
	:	
	_camera()
{
	_camera.NearPlane = 100;
	_camera.FarPlane = 12000;
}

Scene::~Scene()
{

}

void Scene::Initialize()
{
	InitTerrain();	
}

void Scene::OnWindowResize(int width, int height)
{
	_camera.OnWindowResize(width, height);
	_ship.GetCamera().OnWindowResize(width, height);
}

void Scene::InitTerrain()
{
	terrain.loadData("data/textures/heightmap4.bmp", "data/textures/terrain4.bmp");	
}

void Scene::Update(float deltaTime, const Input& input)
{
	terrain.Update(deltaTime);

	if (!_gameMenu.IsActive())
	{
		_ship.Update(deltaTime, input);
	}

	_gameMenu.Update(deltaTime, input);
}

void Scene::Render()
{
	Camera& camera = _camera;

	if (!_gameMenu.IsActive())
	{
		camera = _ship.GetCamera();
	}

	camera.StartFrame();

	_skybox.Render(camera);

	_ship.Render(camera);

	terrain.Render(camera);

	if (!_gameMenu.IsActive())
	{
		_shipHUD.Render(_ship);
	}
	else
	{
		_gameMenu.Render(camera);
	}
}



