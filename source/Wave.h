/**************************************************************/
/* File : Max_Wave.h										  */
/* Developed : Manox (manox@atlas.cz)						  */
/* Date : 1.8.2005											  */
/**************************************************************/

#ifndef _SEAWAVE_H
#define _SEAWAVE_H

#include "Vector.h"
#include "Texture.h"

class SeaWave
{
public:
	Vector3f pocatek1;
	Vector3f pocatek2;
	Vector3f konec1;
	Vector3f konec2;
	Vector3f zarazka1;
	Vector3f zarazka2;
	Vector3f roh1;
	Vector3f roh2;
	Vector3f roh3;
	Vector3f roh4;
	Vector3f roh5;
	Vector3f roh6;
	Vector3f roh7;
	Vector3f roh8;
	Texture textura;
	float t;
	bool direction;
	float korekce;

	SeaWave(Vector3f poc1, Vector3f poc2, Vector3f kon1, Vector3f kon2);

	void Update(float deltaTime);
	void Render() const;
};

#endif