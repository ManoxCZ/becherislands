/**************************************************************/
/* File : Max_Vector.h										  */
/* Developed : Manox (manox@atlas.cz)						  */
/* Date : 16.5.2005											  */
/**************************************************************/

#ifndef _VECTOR_H
#define _VECTOR_H

class Vector2f
{
public:
	float X;
	float Y;	

	Vector2f();
	Vector2f(float x, float y);

	Vector2f operator*(Vector2f other);
	Vector2f operator*(float c);
	Vector2f operator-(Vector2f other);
	Vector2f operator+(Vector2f other);
	Vector2f operator-(void);
	Vector2f operator/(Vector2f other);

	void Normalize();
	float Length();
	float Dot(Vector2f other);	
};

Vector2f operator+(const Vector2f& v1, const Vector2f& v2);
Vector2f operator*(const Vector2f& v1, const float& c);


class Vector3f
{
public:
	float X;
	float Y;
	float Z;

	Vector3f();
	Vector3f(float x, float y, float z);

	Vector3f operator*(Vector3f other);
	Vector3f operator*(float c);	
	Vector3f operator-(Vector3f other);
	Vector3f operator+(Vector3f other);
	Vector3f operator-(void);
	Vector3f operator/(Vector3f other);
		
	void Normalize();	
	float Length();
	float Dot(Vector3f other);
	Vector3f Cross(Vector3f other);	
};

Vector3f operator+(const Vector3f& v1, const Vector3f& v2);
Vector3f operator*(const Vector3f& v1, const float& c);

#endif