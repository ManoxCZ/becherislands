/**************************************************************/
/* File : Max_Level.h										  */
/* Developed : Manox (manox@atlas.cz)						  */
/* Date : 14.9.2005											  */
/**************************************************************/

#ifndef _LEVEL_H
#define _LEVEL_H

#include <string>
#include <deque>

class Ukol
{
public:
	unsigned int molo;
	std::string text_1;
	std::string text_2;
	std::string text_3;
	std::string text_4;
	std::string text_5;
	std::string text_6;

	void reset();
};

class Level
{
public:
	std::deque <Ukol> ukoly;
	int mise;
	double time;

	void load_level(const std::string& file);
	void aktualizace(unsigned int ktere);
};

#endif
