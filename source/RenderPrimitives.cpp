#include "RenderPrimitives.h"
#include <windows.h>
#include <gl\gl.h>

void RenderPrimitives::RenderQuad2D(const Vector2f& position, const Vector2f& size)
{
	glBegin(GL_QUADS);
	
	glTexCoord2f(0.0, 0.0);
	glVertex2f(position.X, position.Y);
	
	glTexCoord2f(0.0, 1.0);
	glVertex2f(position.X, position.Y + size.Y);
	
	glTexCoord2f(1.0, 1.0);
	glVertex2f(position.X + size.X, position.Y + size.Y);
	
	glTexCoord2f(1.0, 0.0);
	glVertex2f(position.X + size.X, position.Y);
	
	glEnd();
}