#include "Camera.h"
#include <windows.h>
#include <gl\gl.h>
#include <gl\glu.h>



Camera::Camera()
	:
	Width(800),
	Height(600),
	Position(0, 500, 0),
	Target(10, 500, 10),
	UpVector(0, 1, 0),
	AspectRatio(1),
	NearPlane(1.0f),
	FarPlane(15000.0f),
	FieldOfView(45),
	ClearColor(Color(1, 0, 0, 1)),
	ClearDepth(1.0f)
{

}

void Camera::OnWindowResize(int width, int height)
{
	Width = width;
	Height = height;

	AspectRatio = Width / (float)Height;
}

void Camera::StartFrame() const
{
	glMatrixMode(GL_PROJECTION);

	glLoadIdentity();
	glViewport(0, 0, Width, Height);
	gluPerspective(FieldOfView, AspectRatio, NearPlane, FarPlane);

	glMatrixMode(GL_MODELVIEW);

	glClearColor(ClearColor.R, ClearColor.G, ClearColor.B, ClearColor.A);
	glClearDepth(ClearDepth);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
}

void Camera::SetLookAt() const
{
	gluLookAt(
		Position.X, Position.Y, Position.Z,
		Target.X, Target.Y, Target.Z,
		UpVector.X, UpVector.Y, UpVector.Z);
}

void Camera::SetLookAtRotationOnly() const
{
	gluLookAt(
		0,0,0,
		Target.X - Position.X,
		Target.Y - Position.Y,
		Target.Z - Position.Z,
		UpVector.X, UpVector.Y, UpVector.Z);
}

