/**************************************************************/
/* File : Max_Credits.h										  */
/* Developed : Manox (manox@atlas.cz)						  */
/* Date : 16.9.2005											  */
/**************************************************************/

#ifndef _CREDITS_H
#define _CREDITS_H

class Titulky
{
public:
	float x;

	void init();
	void start_music();
	void render();
};

#endif
