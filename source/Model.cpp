/**************************************************************/
/* File : Max_Model.cpp										  */
/* Developed : Manox (manox@atlas.cz)						  */
/* Date : 16.5.2005											  */
/**************************************************************/

#include "Model.h"
#include "Texture.h"
#include "tiny_obj_loader.h"
#include <filesystem>
#include <windows.h>
#include <gl\gl.h>
#include <gl\glu.h>


Model::Model(const std::string& filename, const std::string& lodFilename)
{	
	LoadOBJ(filename, 0);

	if (lodFilename.size() != 0)
	{
		LoadOBJ(lodFilename, 1);
	}
}

void Model::Render(const std::vector<Mesh>& meshes) const
{
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);

	glVertexPointer(3, GL_FLOAT, 0, _vertices.data());
	glNormalPointer(GL_FLOAT, 0, _normals.data());
	glTexCoordPointer(2, GL_FLOAT, 0, _texCoord0.data());
	
	glEnable(GL_TEXTURE_2D);

	for (unsigned int i = 0; i < meshes.size(); i++)
	{
		meshes[i].MainTexture.Bind();

		glDrawArrays(GL_TRIANGLES, meshes[i].StartVertex, meshes[i].VerticesCount);
	}

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
}

void Model::Render() const
{
	Render(_meshes);
}

void Model::RenderLOD() const
{
	Render(_meshesLOD);
}


const Model& Model::GetModel(const std::string& filename, const std::string& lodFilename)
{
	if (_models.size() == 0 || _models.find(filename) == _models.end())
	{
		_models.emplace(filename, Model(filename, lodFilename));
	}

	return _models.at(filename);
}

bool Model::LoadOBJ(const std::string& filename, unsigned int level)
{
	tinyobj::ObjReader reader;

	std::filesystem::path folder(filename);

	if (reader.ParseFromFile(filename))
	{ 
		for (const tinyobj::shape_t& shape : reader.GetShapes())
		{
			Mesh newMesh;
			newMesh.StartVertex = _vertices.size();
			newMesh.VerticesCount = shape.mesh.indices.size();
			
			tinyobj::material_t material = reader.GetMaterials()[shape.mesh.material_ids[0]];
			
			std::filesystem::path texture(material.diffuse_texname);
			const std::wstring& texturePath = (folder.parent_path() / texture).c_str();
			std::string str(texturePath.begin(), texturePath.end());
			newMesh.MainTexture = Texture::GetTexture(str);

			for (size_t i = 0; i < shape.mesh.indices.size(); i++)
			{
				tinyobj::index_t index = shape.mesh.indices[i];

				_vertices.push_back(std::move(
					Vector3f(
						reader.GetAttrib().vertices[3 * index.vertex_index],
						reader.GetAttrib().vertices[3 * index.vertex_index + 1],
						reader.GetAttrib().vertices[3 * index.vertex_index + 2])));

				_normals.push_back(std::move(
					Vector3f(
						reader.GetAttrib().normals[3 * index.normal_index],
						reader.GetAttrib().normals[3 * index.normal_index + 1],
						reader.GetAttrib().normals[3 * index.normal_index + 2])));

				_texCoord0.push_back(std::move(
					Vector2f(
						reader.GetAttrib().texcoords[2 * index.texcoord_index],
						reader.GetAttrib().texcoords[2 * index.texcoord_index + 1])));
			}

			_meshes.push_back(std::move(newMesh));
		}

		return true;
	}

	return false;
}



ModelInstance::ModelInstance(const Model& model)
	: _model(model)
{

}

ModelInstance::ModelInstance(const ModelInstance& modelInstance)
	:
	_model(modelInstance._model),
	Position(modelInstance.Position),
	Rotation(modelInstance.Rotation),
	Scale(modelInstance.Scale)
{
	
}

void ModelInstance::Render(const Camera& camera) const
{
	glLoadIdentity();

	camera.SetLookAt();

	glTranslatef(Position.X, Position.Y, Position.Z);
	
	glRotatef(Rotation.X, 1.0f, 0.0f, 0.0f);
	glRotatef(Rotation.Y, 0.0f, 1.0f, 0.0f);
	glRotatef(Rotation.Z, 0.0f, 0.0f, 1.0f);
	
	glScalef(Scale.X, Scale.Y, Scale.Z);
	
	/*GLdouble eqn[] = { 0.0,1.0,0.0,position.Y };
	glClipPlane(GL_CLIP_PLANE0, eqn);*/

	

	_model.Render();

	/*GLdouble eqn1[] = { 0.0,1.0,0.0,0.0 };
	glClipPlane(GL_CLIP_PLANE0, eqn1);*/

	//glDisable(GL_CULL_FACE);
}

void ModelInstance::RenderLOD(const Camera& camera) const
{
	/*GLdouble eqn[] = { 0.0,1.0,0.0,position.Y };
	glClipPlane(GL_CLIP_PLANE0, eqn);*/

	glLoadIdentity();

	camera.SetLookAt();

	glTranslatef(Position.X, Position.Y, Position.Z);

	glRotatef(Rotation.X, 1.0f, 0.0f, 0.0f);
	glRotatef(Rotation.Y, 0.0f, 1.0f, 0.0f);
	glRotatef(Rotation.Z, 0.0f, 0.0f, 1.0f);

	glScalef(Scale.X, Scale.Y, Scale.Z);

	_model.RenderLOD();

	/*GLdouble eqn1[] = { 0.0,1.0,0.0,0.0 };
	glClipPlane(GL_CLIP_PLANE0, eqn1);*/
}