/**************************************************************/
/* File : Max_Particle.cpp									  */
/* Developed : Manox (manox@atlas.cz)						  */
/* Date : 8.6.2005											  */
/**************************************************************/

#include "Particle.h"
#include "Max_Math.h"
#include <windows.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include <gl\glut.h>

Particle::Particle(float totalLifetime, 
	const Color& startColor, const Color& endColor,
	float startSize, float endSize)
	:
	_totalLifetime(totalLifetime),
	_startColor(startColor),
	_endColor(endColor),
	_startSize(startSize),
	_endSize(endSize),	
	_lifetime(-1.0f)	
{		
}

void Particle::Render() const
{
	GLfloat modelview[16];	
	
	glPushMatrix();

	Vector3f position = _position + _velocity * (_totalLifetime - _lifetime);

	glTranslatef(position.X, position.Y, position.Z);

	glGetFloatv(GL_PROJECTION_MATRIX, modelview);
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++) {
			if (i == j)
				modelview[i * 4 + j] = 1.0;
			else
				modelview[i * 4 + j] = 0.0;
		}
	glLoadMatrixf(modelview);	

	float lifeFactor = _lifetime / _totalLifetime;

	glColor3f(
		lerp(_endColor.R, _startColor.R, lifeFactor),
		lerp(_endColor.G, _startColor.G, lifeFactor),
		lerp(_endColor.B, _startColor.B, lifeFactor));

	float size = lerp(_endSize, _startSize, lifeFactor);

	glBegin(GL_TRIANGLE_STRIP);		
		glTexCoord2d(1, 1); glVertex3f(size, size, 0);
		glTexCoord2d(0, 1); glVertex3f(-size, size, 0);
		glTexCoord2d(1, 0); glVertex3f(size, -size, 0);
		glTexCoord2d(0, 0); glVertex3f(-size, -size, 0);
	glEnd();

	glPopMatrix();

	glColor3f(1.0f, 1.0f, 1.0f);
}

void Particle::Update(float deltaTime)
{
	_lifetime -= deltaTime;	
}

bool Particle::IsAlive() const
{
	return _lifetime > 0;
}

void Particle::Spawn(const Vector3f& position, const Vector3f& velocity)
{
	_position = position;
	_velocity = velocity;
	_lifetime = _totalLifetime;
}