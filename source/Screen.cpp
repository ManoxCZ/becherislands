#include "Screen.h"
#include <algorithm>

Screen::Screen()
	:
	_backButtonPressed(false),
	_nextButtonPressed(false),
	_actionButtonPressed(false)
{

}

MenuAction Screen::Update(float deltaTime, const Input& input)
{
	MenuAction result(ActionType::None);

	UpdateKeyboardInput(input, result);

	return result;
}

void Screen::UpdateKeyboardInput(const Input& input, MenuAction& action)
{
	bool backButtonPressed = input.IsSpecialKeyPressed(GLUT_KEY_UP);
	bool nextButtonPressed = input.IsSpecialKeyPressed(GLUT_KEY_DOWN);
	bool actionButtonPressed = input.IsKeyPressed((unsigned char)13);

	if (_backButtonPressed && !backButtonPressed)
	{
		MoveBack();
	}

	if (_nextButtonPressed && !nextButtonPressed)
	{
		MoveNext();
	}

	if (_actionButtonPressed && !actionButtonPressed)
	{
		action = Buttons.at(GetActiveButton()).MenuAction;
	}

	_backButtonPressed = backButtonPressed;
	_nextButtonPressed = nextButtonPressed;
	_actionButtonPressed = actionButtonPressed;
}

void Screen::MoveBack()
{
	int activeButton = GetActiveButton();

	int nextActiveButton = std::max(0, activeButton - 1);

	if (activeButton != nextActiveButton)
	{
		Buttons.at(activeButton).IsSelected = false;
		Buttons.at(nextActiveButton).IsSelected = true;
	}
}

void Screen::MoveNext()
{
	int activeButton = GetActiveButton();

	int nextActiveButton = std::min((int)Buttons.size() - 1, activeButton + 1);

	if (activeButton != nextActiveButton)
	{
		Buttons.at(activeButton).IsSelected = false;
		Buttons.at(nextActiveButton).IsSelected = true;
	}
}

int Screen::GetActiveButton() const
{
	for (size_t i = 0; i < Buttons.size(); i++)
	{
		if (Buttons.at(i).IsSelected)
		{
			return i;
		}
	}

	return 0;
}

void Screen::Render(const Camera& camera) const
{
	for (const Button& button : Buttons)
	{
		button.Render();
	}
}