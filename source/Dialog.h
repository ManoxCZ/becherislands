/**************************************************************/
/* File : Max_GUI.h											  */
/* Developed : Manox (manox@atlas.cz)						  */
/* Date : 14.9.2005											  */
/**************************************************************/

#ifndef _GUI_H
#define _GUI_H

#include <string>

class Dialog
{
public:
	float px, py, kx, ky;
	std::string text_1;
	std::string text_2;
	std::string text_3;
	std::string text_4;
	std::string text_5;
	std::string text_6;

	void init(float ipx, float ipy, float ikx, float iky);
	void init_text(const std::string& itext_1, const std::string& itext_2, const std::string& itext_3,
		const std::string& itext_4, const std::string& itext_5, const std::string& itext_6);
	void render();
};

#endif
