#pragma once

#include "Vector.h"

class RenderPrimitives
{
public:
	static void RenderQuad2D(const Vector2f& position, const Vector2f& size);
};