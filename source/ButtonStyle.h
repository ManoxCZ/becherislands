#pragma once

#include "Color.h"

struct ButtonStyle
{
public:
	Color NormalColor = Color(1);
	Color SelectedColor = Color(1, 0.25f, 0.25f);	
};
