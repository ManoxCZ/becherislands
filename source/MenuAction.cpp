#include "MenuAction.h"


MenuAction::MenuAction()
	:
	Type(ActionType::None),
	ScreenId(-1)
{

}

MenuAction::MenuAction(ActionType type, int screenId)
	:
	Type(type),
	ScreenId(screenId)
{

}
