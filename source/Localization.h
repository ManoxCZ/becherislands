#pragma once

#include "LocalizedString.h"
#include <string>
#include <map>
#include <functional>


class Localization
{
private:
	LocalizedString _notFoundString;
	std::map<std::string, LocalizedString> _strings;


public:
	Localization();

	const LocalizedString& GetLocalizedString(const std::string& key) const;
};