/**************************************************************/
/* File : Max_Particle_Emiter.cpp							  */
/* Developed : Manox (manox@atlas.cz)						  */
/* Date : 8.6.2005											  */
/**************************************************************/

#include "Particle_Emiter.h"
#include <windows.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include <gl\glut.h>


ParticleEmitter::ParticleEmitter(float totalLifetime, 
	const Color& startColor, const Color& endColor, 
	float startSize, float endSize, 	
	int maxParticlesCount)
	: 
	_actualParticle(0),
	_spawningTime(0)
{
	for (size_t i = 0; i < maxParticlesCount; i++)
		_particles.push_back(Particle(totalLifetime, startColor, endColor, startSize, endSize));
}

ParticleEmitter::~ParticleEmitter(void)
{
}

void ParticleEmitter::Initialize()
{
	_texture = Texture::GetTexture("data/textures/Particle.bmp");
}

void ParticleEmitter::SpawnParticles(int count)
{
	for (int i = 0; i < count; i++)
	{
		if (!_particles[_actualParticle].IsAlive())
		{
			/*float uhelx = Ux - Uxo + 2 * Uxo*(rand()) / RAND_MAX;
			float uhelz = Uz - Uzo + 2 * Uzo*(rand()) / RAND_MAX;
			float r = Rychlost;

			float uX = (2 * M_PI / 360)*uhelx;
			float uZ = (2 * M_PI / 360)*uhelz;*/

			_particles[_actualParticle].Spawn(Vector3f(0, 0, 0), Velocity);
			
			_actualParticle = (_actualParticle + 1) % _particles.size();
		}
	}
}
 
 void ParticleEmitter::Update(float deltaTime)
 {
	 _spawningTime += deltaTime;
	 
	 float spawnTime = 1.0f / SpawnRatio;	 

	 int spawnParticles = (int)(_spawningTime / spawnTime);

	 if (spawnParticles > 0)
	 {
		 SpawnParticles(spawnParticles);

		 _spawningTime -= spawnParticles * spawnTime;
	 }

	 for (Particle& particle : _particles)
	 {
		 if (particle.IsAlive())
			 particle.Update(deltaTime);
	 }
 }

 void ParticleEmitter::Render() const
 {
	 _texture.Bind();

	 glDisable(GL_LIGHTING);

	 glEnable(GL_BLEND);
	 glBlendFunc(GL_ONE_MINUS_SRC_COLOR, GL_SRC_COLOR);

	 glPushMatrix();
	 glTranslatef(Position.X, Position.Y, Position.Z);

	 for (const Particle& particle : _particles)
	 {
		 if (particle.IsAlive())
		 {
			 particle.Render();
		 }
	 }

	 glPopMatrix();

	 glEnable(GL_LIGHTING);
	 glDisable(GL_BLEND);
 }

 void ParticleEmitter::Reset()
 {

 }
