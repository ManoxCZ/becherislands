#include "Input.h"


void Input::OnKeyPressed(unsigned char keyCode)
{
	_keys[keyCode] = true;
}

void Input::OnKeyReleased(unsigned char  keyCode)
{
	_keys[keyCode] = false;
}

void Input::OnSpecialKeyPressed(int keyCode)
{
	_specialKeys.insert(keyCode);
}

void Input::OnSpecialKeyReleased(int keyCode)
{
	_specialKeys.erase(keyCode);
}

void Input::OnGamepadPressed(unsigned int code, int x, int y, int z)
{
	_gamepad[code] = std::move(Vector3f(x / 1000.0f, y / 1000.0f, z / 1000.0f));
}

bool Input::IsKeyPressed(unsigned char keyCode) const
{
	return _keys[keyCode];
}

bool Input::IsSpecialKeyPressed(int keyCode) const
{
	return _specialKeys.contains(keyCode);
}

const Vector3f& Input::GetGamepadButtonState(unsigned int code) const
{
	return _gamepad.at(code);
}