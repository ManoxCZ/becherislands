#pragma once

#include "Ship.h"
#include "Texture.h"

class HUD
{
private:
	const Texture& _speedBackground;
	const Texture& textura_zdravi;
	const Texture& textura_kompas;
	const Texture& textura_powered;
	const Texture& textura_mapa;
	const Texture& textura_kurzor;
	const Texture& textura_podtext;
	const Texture& textura_zatext;
	const Texture& textura_logo;
	const Texture& textura_info;
	const Texture& textura_keen;
	std::string goal_1;
	std::string goal_2;
	std::string goal_3;
	std::string time;


public:
	HUD();

	void Render(const Ship& ship) const;
};