#pragma once

enum class ActionType
{
	None,
	PushScreen,
	PopScreen
};


struct MenuAction
{
	ActionType Type;
	int ScreenId;

	MenuAction();
	MenuAction(ActionType type, int screenId = -1);
};