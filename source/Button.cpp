#include "Button.h"
#include "Text.h"


Button::Button(const ButtonStyle& style, const Vector2f& position, const Vector2f& size, const std::string& text)
	:	
	Style(style),
	MenuAction(ActionType::None),
	Position(position),
	Size(size),
	IsSelected(false),
	Text(text)
{
}

void Button::Render() const
{
	if (IsSelected)
	{
		RenderText(Text, Position, Style.SelectedColor);
	}
	else
	{
		RenderText(Text, Position, Style.NormalColor);
	}	
}