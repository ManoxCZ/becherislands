/**************************************************************/
/* File : Max_Skybox.h										  */
/* Developed : Manox (manox@atlas.cz)						  */
/* Date : 8.6.2005											  */
/**************************************************************/

#ifndef _SKYBOX_H
#define _SKYBOX_H

#include "Camera.h"
#include "Texture.h"

class Skybox
{
private:
	const float SIZE = 2000;
	const float HEIGHT = 500;

	Texture _top;
	Texture _back;
	Texture _front;
	Texture _right;
	Texture _left;
	Texture _bottom;
	Texture _sky;	

public:
	Skybox();
	void Render(const Camera& camera) const;
	void RenderWaterReflection();
};

#endif