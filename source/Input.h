#pragma once

#include "Vector.h"
#include <map>
#include <set>
#include <gl\glut.h>

class Input
{
private:
	bool _keys[256];
	std::set<int> _specialKeys;
	std::map<unsigned int, Vector3f> _gamepad;

public:
	void OnKeyPressed(unsigned char keyCode);
	void OnKeyReleased(unsigned char  keyCode);

	void OnSpecialKeyPressed(int keyCode);
	void OnSpecialKeyReleased(int keyCode);

	void OnGamepadPressed(unsigned int code, int x, int y, int z);

	bool IsKeyPressed(unsigned char keyCode) const;
	bool IsSpecialKeyPressed(int keyCode) const;
	const Vector3f& GetGamepadButtonState(unsigned int code) const;
};