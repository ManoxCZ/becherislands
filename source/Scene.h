#pragma once

#include "Camera.h"
#include "HUD.h"
#include "Input.h"
#include "Terrain.h"
#include "Skybox.h"
#include "Ship.h"
#include "Menu.h"

class Scene
{
private:	
	Camera _camera;
	Skybox _skybox;
	Menu _gameMenu;
	Ship _ship;
	HUD _shipHUD;
	
public:
	Terrain terrain;
	
	Ship jachta_menu;
	
	Scene();
	~Scene();

	Ship& GetShip() { return _ship; }

	void Initialize();
	void OnWindowResize(int width, int height);
	void Update(float deltaTime, const Input& input);
	void Render();
	

private:
	void InitTerrain();
	void onDisplay(const Camera& camera, float deltaTime);	
};

