/**************************************************************/
/* File : Max_Vector.cpp									  */
/* Developed : Manox (manox@atlas.cz)						  */
/* Date : 16.5.2005											  */
/**************************************************************/

#include "Vector.h"
#include <math.h>


Vector2f::Vector2f()
	:
	X(0),
	Y(0)
{
}

Vector2f::Vector2f(float x, float y)
	:
	X(x),
	Y(y)
{
}

Vector2f Vector2f::operator*(Vector2f other)
{
	return Vector2f(X * other.X, Y * other.Y);
}

Vector2f Vector2f::operator/(Vector2f other)
{
	return Vector2f(X / other.X, Y / other.Y);
}

Vector2f Vector2f::operator-(void)
{
	return Vector2f(-X, -Y);
}

Vector2f Vector2f::operator-(Vector2f other)
{
	return Vector2f(X - other.X, Y - other.Y);
}

Vector2f Vector2f::operator+(Vector2f other)
{
	return Vector2f(X + other.X, Y + other.Y);
}

Vector2f Vector2f::operator*(float c)
{
	return Vector2f(X * c, Y * c);
}

void Vector2f::Normalize()
{
	float length = Length();

	X = X / length;
	Y = Y / length;	
}

float Vector2f::Length()
{
	return sqrt(X * X + Y * Y);
}

float Vector2f::Dot(Vector2f other)
{
	return (X * other.X + Y * other.Y);
}

Vector2f operator+(const Vector2f& v1, const Vector2f& v2)
{
	return Vector2f(v1.X + v2.X, v1.Y + v2.Y);
}

Vector2f operator*(const Vector2f& v1, const float& c)
{
	return Vector2f(v1.X * c, v1.Y * c);
}



Vector3f::Vector3f()
	:
	X(0),
	Y(0),
	Z(0)
{
}

Vector3f::Vector3f(float x, float y, float z)
	:
	X(x),
	Y(y),
	Z(z)
{
}

Vector3f Vector3f::operator*(Vector3f other)
{
	return Vector3f(X * other.X, Y * other.Y, Z * other.Z);
}

Vector3f Vector3f::operator/(Vector3f other)
{
	return Vector3f(X / other.X, Y / other.Y, Z / other.Z);
}

Vector3f Vector3f::operator-(void)
{
	return Vector3f(-X, -Y, -Z);
}

Vector3f Vector3f::operator-(Vector3f other)
{
	return Vector3f(X - other.X, Y - other.Y, Z - other.Z);
}

Vector3f Vector3f::operator+(Vector3f other)
{
	return Vector3f(X + other.X, Y + other.Y, Z + other.Z);
}

Vector3f Vector3f::operator*(float c)
{
	return Vector3f(X * c, Y * c, Z * c);
}

void Vector3f::Normalize()
{
	float length = Length();

	X = X / length;
	Y = Y / length;
	Z = Z / length;
}

float Vector3f::Length()
{
	return sqrt(X * X + Y * Y + Z * Z);	
}

float Vector3f::Dot(Vector3f other)
{
	return (X * other.X + Y * other.Y + Z * other.Z);
}

Vector3f Vector3f::Cross(Vector3f other)
{
	return Vector3f(Y * other.Z - Z * other.Y, Z * other.X - X * other.Z, X * other.Y - Y * other.X);
}

Vector3f operator+(const Vector3f& v1, const Vector3f& v2)
{
	return Vector3f(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z);
}

Vector3f operator*(const Vector3f& v1, const float& c)
{
	return Vector3f(v1.X * c, v1.Y * c, v1.Z * c);
}
