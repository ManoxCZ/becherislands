#include "Dock.h"
#include "Max_Math.h"


Molo::Molo(const Vector3f& position, const Vector3f& rotation, unsigned int id, unsigned int typ)
	:
	ID(id),
	Position(position),
	Rotation(rotation)
{
	float nejm_x, nejm_y, nejv_x, nejv_y = 0;

	if (typ == 0)
	{
		nejm_x = -23.0318f;
		nejm_y = -29.884f;
		nejv_x = -5.38001f;
		nejv_y = 26.9925f;
	}
	else if (typ == 1)
	{
		nejm_x = 0;
		nejm_y = 0;
		nejv_x = 0;
		nejv_y = 0;
	}
	else if (typ == 2)
	{
		nejm_x = -10.294f;
		nejm_y = -56.3741f;
		nejv_x = 9.706f;
		nejv_y = 43.6259f;
	}
	else if (typ == 3)
	{
		nejm_x = -5.45916f;
		nejm_y = -26.2655f;
		nejv_x = 3.795f;
		nejv_y = 4.02911f;
	}

	kol_A = Vector2f(
		Position.X + nejm_x * cos(-DegToRad(Rotation.Y)) - nejm_y * sin(-DegToRad(Rotation.Y)),
		Position.Z + nejm_x * sin(-DegToRad(Rotation.Y)) + nejm_y * cos(-DegToRad(Rotation.Y)));
	
	kol_B = Vector2f(
		Position.X + nejv_x * cos(-DegToRad(Rotation.Y)) - nejm_y * sin(-DegToRad(Rotation.Y)),
		Position.Z + nejv_x * sin(-DegToRad(Rotation.Y)) + nejm_y * cos(-DegToRad(Rotation.Y)));
	
	kol_C = Vector2f(
		Position.X + nejv_x * cos(-DegToRad(Rotation.Y)) - nejv_y * sin(-DegToRad(Rotation.Y)),
		Position.Z + nejv_x * sin(-DegToRad(Rotation.Y)) + nejv_y * cos(-DegToRad(Rotation.Y)));
	
	kol_D = Vector2f(
		Position.X + nejm_x * cos(-DegToRad(Rotation.Y)) - nejv_y * sin(-DegToRad(Rotation.Y)),
		Position.Z + nejm_x * sin(-DegToRad(Rotation.Y)) + nejv_y * cos(-DegToRad(Rotation.Y)));	
}

bool Molo::CollideWithShip(const Ship& ship) const
{
	float a1 = ship.kol_A.X - kol_A.X;
	float b1 = ship.kol_A.Y - kol_A.Y;
	float a2 = ship.kol_A.X - kol_B.X;
	float b2 = ship.kol_A.Y - kol_B.Y;

	float t1 = ((-b1 * kol_B.X + a1 * kol_B.Y - (-b1 * ship.kol_A.X + a1 * ship.kol_A.Y)) /
		(b1 * (kol_C.X - kol_B.X) - a1 * (kol_C.Y - kol_B.Y)));
	float t2 = ((-b2 * kol_C.X + a2 * kol_C.Y - (-b2 * ship.kol_A.X + a2 * ship.kol_A.Y)) /
		(b2 * (kol_A.X - kol_C.X) - a2 * (kol_A.Y - kol_C.Y)));
	float t3 = ((-b1 * kol_B.X + a1 * kol_B.Y - (-b1 * ship.kol_A.X + a1 * ship.kol_A.Y)) /
		(b1 * (kol_D.X - kol_B.X) - a1 * (kol_D.Y - kol_B.Y)));
	float t4 = ((-b2 * kol_D.X + a2 * kol_D.Y - (-b2 * ship.kol_A.X + a2 * ship.kol_A.Y)) /
		(b2 * (kol_A.X - kol_D.X) - a2 * (kol_A.Y - kol_D.Y)));

	if ((t1 > 0 && t1 < 1 && t2>0 && t2 < 1) || (t3 > 0 && t3 < 1 && t4>0 && t4 < 1))
	{
		return true;
	}
	else
	{
		a1 = ship.kol_B.X - kol_A.X;
		b1 = ship.kol_B.Y - kol_A.Y;
		a2 = ship.kol_B.X - kol_B.X;
		b2 = ship.kol_B.Y - kol_B.Y;

		t1 = ((-b1 * kol_B.X + a1 * kol_B.Y - (-b1 * ship.kol_B.X + a1 * ship.kol_B.Y)) /
			(b1 * (kol_C.X - kol_B.X) - a1 * (kol_C.Y - kol_B.Y)));
		t2 = ((-b2 * kol_C.X + a2 * kol_C.Y - (-b2 * ship.kol_B.X + a2 * ship.kol_B.Y)) /
			(b2 * (kol_A.X - kol_C.X) - a2 * (kol_A.Y - kol_C.Y)));
		t3 = ((-b1 * kol_B.X + a1 * kol_B.Y - (-b1 * ship.kol_B.X + a1 * ship.kol_B.Y)) /
			(b1 * (kol_D.X - kol_B.X) - a1 * (kol_D.Y - kol_B.Y)));
		t4 = ((-b2 * kol_D.X + a2 * kol_D.Y - (-b2 * ship.kol_B.X + a2 * ship.kol_B.Y)) /
			(b2 * (kol_A.X - kol_D.X) - a2 * (kol_A.Y - kol_D.Y)));

		if ((t1 > 0 && t1 < 1 && t2>0 && t2 < 1) || (t3 > 0 && t3 < 1 && t4>0 && t4 < 1))
		{
			return true;
		}
		else
		{
			a1 = ship.kol_C.X - kol_A.X;
			b1 = ship.kol_C.Y - kol_A.Y;
			a2 = ship.kol_C.X - kol_B.X;
			b2 = ship.kol_C.Y - kol_B.Y;

			t1 = ((-b1 * kol_B.X + a1 * kol_B.Y - (-b1 * ship.kol_C.X + a1 * ship.kol_C.Y)) /
				(b1 * (kol_C.X - kol_B.X) - a1 * (kol_C.Y - kol_B.Y)));
			t2 = ((-b2 * kol_C.X + a2 * kol_C.Y - (-b2 * ship.kol_C.X + a2 * ship.kol_C.Y)) /
				(b2 * (kol_A.X - kol_C.X) - a2 * (kol_A.Y - kol_C.Y)));
			t3 = ((-b1 * kol_B.X + a1 * kol_B.Y - (-b1 * ship.kol_C.X + a1 * ship.kol_C.Y)) /
				(b1 * (kol_D.X - kol_B.X) - a1 * (kol_D.Y - kol_B.Y)));
			t4 = ((-b2 * kol_D.X + a2 * kol_D.Y - (-b2 * ship.kol_C.X + a2 * ship.kol_C.Y)) /
				(b2 * (kol_A.X - kol_D.X) - a2 * (kol_A.Y - kol_D.Y)));

			if ((t1 > 0 && t1 < 1 && t2>0 && t2 < 1) || (t3 > 0 && t3 < 1 && t4>0 && t4 < 1))
			{
				return true;
			}
			else
			{
				a1 = ship.kol_D.X - kol_A.X;
				b1 = ship.kol_D.Y - kol_A.Y;
				a2 = ship.kol_D.X - kol_B.X;
				b2 = ship.kol_D.Y - kol_B.Y;

				t1 = ((-b1 * kol_B.X + a1 * kol_B.Y - (-b1 * ship.kol_D.X + a1 * ship.kol_D.Y)) /
					(b1 * (kol_C.X - kol_B.X) - a1 * (kol_C.Y - kol_B.Y)));
				t2 = ((-b2 * kol_C.X + a2 * kol_C.Y - (-b2 * ship.kol_D.X + a2 * ship.kol_D.Y)) /
					(b2 * (kol_A.X - kol_C.X) - a2 * (kol_A.Y - kol_C.Y)));
				t3 = ((-b1 * kol_B.X + a1 * kol_B.Y - (-b1 * ship.kol_D.X + a1 * ship.kol_D.Y)) /
					(b1 * (kol_D.X - kol_B.X) - a1 * (kol_D.Y - kol_B.Y)));
				t4 = ((-b2 * kol_D.X + a2 * kol_D.Y - (-b2 * ship.kol_D.X + a2 * ship.kol_D.Y)) /
					(b2 * (kol_A.X - kol_D.X) - a2 * (kol_A.Y - kol_D.Y)));

				if ((t1 > 0 && t1 < 1 && t2>0 && t2 < 1) || (t3 > 0 && t3 < 1 && t4>0 && t4 < 1))
				{
					return true;
				}
			}
		}		
	}

	return false;

	/*
		glDisable(GL_LIGHTING);
		glColor3f(1,0,0);

		glBegin(GL_LINES);
			glVertex3f(kol_A.X, 15, kol_A.Y);
			glVertex3f(kol_B.X, 15, kol_B.Y);
			glVertex3f(kol_B.X, 15, kol_B.Y);
			glVertex3f(kol_C.X, 15, kol_C.Y);
			glVertex3f(kol_C.X, 15, kol_C.Y);
			glVertex3f(kol_D.X, 15, kol_D.Y);
			glVertex3f(kol_D.X, 15, kol_D.Y);
			glVertex3f(kol_A.X, 15, kol_A.Y);
		glEnd();
		glEnable(GL_LIGHTING);
		glColor3f(1,1,1);
	*/
	/*if (ship.ext_kolize == true)
	{
		out << "!!! EXTERNI KOLIZE S MOLEM : " << ID << std::endl;
		out << "     " << ship.pozice.X << " : " << ship.pozice.Z << " : " << ship.uhel.Y << std::endl;
	}*/


	//if ((sqrt((Position.X - ship.pozice.X) * (Position.X - ship.pozice.X) +
	//	(Position.Z - ship.pozice.Z) * (Position.Z - ship.pozice.Z)) < 60) &&
	//	abs(ship.GetVelocity()) < 10) mise.aktualizace(ID);
}