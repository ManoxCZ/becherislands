#pragma once


struct Color
{
	float R;
	float G;
	float B;
	float A;

	Color()
		:
		R(1.0f),
		G(1.0f),
		B(1.0f),
		A(1.0f)
	{

	}

	Color(float rgb, float a = 1.0f)
		:
		R(rgb),
		G(rgb),
		B(rgb),
		A(a)
	{
	}

	Color(float r, float g, float b, float a = 1.0f)
		:
		R(r),
		G(g),
		B(b),
		A(a)
	{		
	}
};