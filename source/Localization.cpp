#include "Localization.h"


Localization::Localization()
	:
	_notFoundString("Missing string"),
	_strings()
{

}

const LocalizedString& Localization::GetLocalizedString(const std::string& key) const
{
	if (_strings.size() == 0 || _strings.find(key) == _strings.end())
	{
		return _notFoundString;
	}

	return _strings.at(key);
}