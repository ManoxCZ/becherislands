#pragma once

#include "Color.h"
#include "Vector.h"

class Camera
{
public:
	Vector3f Position;
	Vector3f Target;
	Vector3f UpVector;	
	int Width;
	int Height;
	float AspectRatio;
	float NearPlane;
	float FarPlane;
	float FieldOfView;
	Color ClearColor;
	float ClearDepth;

	Camera();

	void StartFrame() const;

	void OnWindowResize(int width, int height);

	void SetLookAt() const;

	void SetLookAtRotationOnly() const;
};