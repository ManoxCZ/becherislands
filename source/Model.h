/**************************************************************/
/* File : Max_Model.h										  */
/* Developed : Manox (manox@atlas.cz)						  */
/* Date : 16.5.2005											  */
/**************************************************************/

#ifndef _MODEL_H
#define _MODEL_H

#include "Texture.h"
#include "Vector.h"
#include "Camera.h"
#include <string>
#include <vector>
#include <map>
#include <functional>


struct Mesh
{
public:
	unsigned int StartVertex;
	unsigned int VerticesCount;
	Texture MainTexture;	
};


class Model
{
private:
	inline static std::map<std::string, Model> _models;

	std::vector<Vector3f> _vertices;
	std::vector<Vector2f> _texCoord0;	
	std::vector<Vector3f> _normals;
	std::vector<Mesh> _meshes;
	std::vector<Mesh> _meshesLOD;	

	Model(const std::string& filename, const std::string& lodFilename);
	bool LoadOBJ(const std::string& filename, unsigned int level);

public:	
	void Render() const;
	void RenderLOD() const;

	static const Model& GetModel(const std::string& filename, const std::string& lodFilename);

private:	
	void Render(const std::vector<Mesh>& meshes) const;
};

class ModelInstance
{
private:
	const Model& _model;

public:
	Vector3f Position;	
	Vector3f Rotation;
	Vector3f Scale;

public:	
	ModelInstance(const Model& model);
	ModelInstance(const ModelInstance& modelInstance);

	void Render(const Camera& camera) const;
	void RenderLOD(const Camera& camera) const;
};

#endif
