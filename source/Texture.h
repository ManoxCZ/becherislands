/**************************************************************/
/* File : Max_Texture.h										  */
/* Developed : Manox (manox@atlas.cz)						  */
/* Date : 25.5.2005											  */
/**************************************************************/

#pragma once

#include <string>
#include <map>
#include <queue>


class Texture
{
private:
	inline static std::queue<int> _freeTextureHandles;
	inline static std::map<std::string, Texture> _textures;


	unsigned int _handle;

	static unsigned int LoadTexture(const std::string& file);

public:

	Texture();
	Texture(unsigned int handle);
	Texture(const Texture& other);

	static const Texture& GetTexture(const std::string& filename);

	void Bind() const;
};

