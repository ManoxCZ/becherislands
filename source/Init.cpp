/**************************************************************/
/* File : Max_Init.cpp										  */
/* Developed : Manox (manox@atlas.cz)						  */
/* Date : 16.5.2005											  */
/**************************************************************/

#include "Init.h"


bool Init()
{
	// OpenGL extension
	/*glActiveTextureARB = (PFNGLACTIVETEXTUREARBPROC)
	wglGetProcAddress("glActiveTextureARB");
	glClientActiveTextureARB = (PFNGLCLIENTACTIVETEXTUREARBPROC)   
	wglGetProcAddress("glClientActiveTextureARB");
	glMultiTexCoord2fARB = (PFNGLMULTITEXCOORD2FARBPROC)
	wglGetProcAddress("glMultiTexCoord2fARB");*/

	//ShowCursor(false);// Nezobrazi kurzor mysi
	
	

	////glEnable(GL_ALPHA_TEST);		/* zapne testovani pruhlednosti */
	////glAlphaFunc(GL_LEQUAL,0);		/* nastaveni parametru pro pruhlednost */

 //   //glCullFace(GL_FRONT);                          /* nastaveni maskovani zadnich polygonu */
	//glDisable(GL_CULL_FACE);                       /* vypnuti maskovani polygonu */
	//
	//glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); /* vylepseni zobrazovani */
    

	// a taky musime inicializovat zvuky

	//fonty

	//BuildFont();										// Build The Font	

	//konec fonty

	/*FSOUND_Init(44000, 0, 0);
	FSOUND_SetOutput(FSOUND_OUTPUT_DSOUND);
	FSOUND_SetMixer(FSOUND_MIXER_AUTODETECT);


	samp1 = FSOUND_Sample_Load(FSOUND_FREE , "data/musics/menu.mp3", 
		FSOUND_NORMAL | FSOUND_HW2D, 0, 0);
    FSOUND_Sample_SetMode(samp1, FSOUND_LOOP_NORMAL);

	hudba_tit = FSOUND_Sample_Load(FSOUND_FREE , "data/musics/credits.mp3", 
		FSOUND_NORMAL | FSOUND_HW2D, 0, 0);
	FSOUND_Sample_SetMode(hudba_tit, FSOUND_LOOP_NORMAL);

	hudba_1 = FSOUND_Sample_Load(FSOUND_FREE , "data/musics/Game01.mp3", 
		FSOUND_NORMAL | FSOUND_HW2D, 0, 0);
	FSOUND_Sample_SetMode(hudba_tit, FSOUND_LOOP_OFF);

	hudba_2 = FSOUND_Sample_Load(FSOUND_FREE , "data/musics/Game02.mp3", 
		FSOUND_NORMAL | FSOUND_HW2D, 0, 0);
	FSOUND_Sample_SetMode(hudba_tit, FSOUND_LOOP_OFF);

	hudba_3 = FSOUND_Sample_Load(FSOUND_FREE , "data/musics/Game03.mp3", 
		FSOUND_NORMAL | FSOUND_HW2D, 0, 0);
	FSOUND_Sample_SetMode(hudba_tit, FSOUND_LOOP_OFF);

	sound_tlac = FSOUND_Sample_Load(FSOUND_FREE , "data/sounds/button.wav", 
		FSOUND_NORMAL | FSOUND_HW2D, 0, 0);

	FSOUND_Sample_SetMode(sound_tlac, FSOUND_LOOP_OFF);

	FSOUND_SetSFXMasterVolume(200);*/

	// konec zvuku

	// klavesnice
	
	// boot
	//Texture texture = Texture::GetTexture("data/textures/loading.bmp");

	//glClear(GL_COLOR_BUFFER_BIT); /* vymazani barvoveho bufferu */    
	//glMatrixMode(GL_PROJECTION);            /* zacatek modifikace projekcni matice */
	//glLoadIdentity(); 
 //   glOrtho(0, 800, 0, 600, -1, 1);             /* mapovani abstraktnich souradnic do souradnic okna */
 //   glScalef(1, -1, 1);                     /* inverze y-ove osy, aby se y zvetsovalo smerem dolu */
 //   glTranslatef(0, -600, 0);                 /* posun pocatku do leveho horniho rohu */

	//glDisable(GL_DEPTH_TEST);
	//glEnable(GL_TEXTURE_2D);
	//
	//texture.Bind();

	//glBegin(GL_QUADS);              /* zacatek zadavani ctverce */
	//	glTexCoord2f(0.0,1.0); 
	//	glVertex2i(0,0);			/* poloha prvniho vertexu */
	//	glTexCoord2f(1.0,1.0);
	//	glVertex2i(1024,0);		/* poloha druheho vertexu */
	//	glTexCoord2f(1.0,0.0);
	//	glVertex2i(1024,1024);/* poloha tretiho vertexu */
	//	glTexCoord2f(0.0,0.0);
	//	glVertex2i(0,1024);		/* poloha ctvrteho vertexu */
	//glEnd();

	//printText("Loading, please wait", font_12, 600, 580, 1,1,1);
	//
	//glutSwapBuffers();
	//// konec boot

	//menu.init();

	//glEnable(GL_DEPTH_TEST);

	

	//HUD.loadInterface();

	// nacitani dat ze souboru

	/*std::ifstream vstup_dat("data/data.txt");
	std::string token; 
	int delka;
	std::string slovo;

	while (std::getline(vstup_dat, token))
	{
		delka = 0;
		if (token[0] != '/')
		{
			slovo="";
			while (token[delka]!='\"') delka++;
			delka++;
			while (token[delka]!='\"') 
			{
				slovo+=token[delka];
				delka++;
			} 
			char *model = new char [slovo.size()+1];
			for (unsigned int kk=0;kk<slovo.size();kk++)
				if (slovo[kk] == '\\') model[kk] = '/';
				else model[kk] = slovo[kk];
			model[slovo.size()] = '\0';
			if (slovo != "") 
			{
				newmodel.reset();
				newmodel.loadASE(model,1,0);
			}

			float cis;
			slovo = "";
			while (!isdigit(token[delka]) && (token[delka]!='.') && (token[delka]!='-')) delka++;
			while (isdigit(token[delka]) || (token[delka]=='.') || (token[delka]=='-')) 
			{
				slovo+=token[delka];
				delka++;
			}
			cis = atof(slovo.c_str());
			newmodel.position.X = cis;

			slovo = "";
			while (!isdigit(token[delka]) && (token[delka]!='.') && (token[delka]!='-')) delka++;
			while (isdigit(token[delka]) || (token[delka]=='.') || (token[delka]=='-')) 
			{
				slovo+=token[delka];
				delka++;
			}
			cis = atof(slovo.c_str());
			newmodel.position.Z = cis;

			slovo = "";
			while (!isdigit(token[delka]) && (token[delka]!='.') && (token[delka]!='-') && (token[delka]!='a')) delka++;
			while (isdigit(token[delka]) || (token[delka]=='.') || (token[delka]=='-') ||
				(token[delka]=='a') || (token[delka]=='u') || (token[delka]=='t') || (token[delka]=='o')) 
			{
				slovo+=token[delka];
				delka++;
			}
			if (slovo[0] == 'a') 
			{
				float kor;
				std::string korek="";
				for (unsigned int ii=4; ii < slovo.length() ;ii++)
				{
					korek+=slovo[ii];
				}
				kor = atof(korek.c_str());

				newmodel.position.Y = teren.height(newmodel.position.X,newmodel.position.Z) + kor;
			}
			else 
			{
				cis = atof(slovo.c_str());
				newmodel.position.Y = cis;
			}

			slovo = "";
			while (!isdigit(token[delka]) && (token[delka]!='.') && (token[delka]!='-')) delka++;
			while (isdigit(token[delka]) || (token[delka]=='.') || (token[delka]=='-')) 
			{
				slovo+=token[delka];
				delka++;
			}
			cis = atof(slovo.c_str());
			newmodel.angle.X = cis;

			slovo = "";
			while (!isdigit(token[delka]) && (token[delka]!='.') && (token[delka]!='-')) delka++;
			while (isdigit(token[delka]) || (token[delka]=='.') || (token[delka]=='-')) 
			{
				slovo+=token[delka];
				delka++;
			}
			cis = atof(slovo.c_str());
			newmodel.angle.Z = cis;

			slovo = "";
			while (!isdigit(token[delka]) && (token[delka]!='.') && (token[delka]!='-')) delka++;
			while (isdigit(token[delka]) || (token[delka]=='.') || (token[delka]=='-')) 
			{
				slovo+=token[delka];
				delka++;
			}
			cis = atof(slovo.c_str());
			newmodel.angle.Y = cis;
	
			scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	
		}
	}*/

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\molo.ASE",2,0);
	//newmodel.position.X = 7372.0f;
	//newmodel.position.Z = 3806.0f;
	//newmodel.position.Y = -10;
	//newmodel.angle.X = 0;
	//newmodel.angle.Y = 0;
	//newmodel.angle.Z = 0;
	//scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);
	//dok.init(newmodel.position.X,newmodel.position.Y,newmodel.position.Z,
	//	newmodel.angle.X,newmodel.angle.Y,newmodel.angle.Z,0,0);
	//doky.push_back(dok);

	//newmodel.position.X = 4345.0f;
	//newmodel.position.Z = 660.0f;
	//newmodel.position.Y = -10;
	//newmodel.angle.X = 0;
	//newmodel.angle.Y = 0;
	//newmodel.angle.Z = 0;
	//scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	
	//dok.init(newmodel.position.X,newmodel.position.Y,newmodel.position.Z,
	//	newmodel.angle.X,newmodel.angle.Y,newmodel.angle.Z,1,0);
	//doky.push_back(dok);

	//newmodel.position.X = 6378.0f;
	//newmodel.position.Z = 1440.0f;
	//newmodel.position.Y = -10;
	//newmodel.angle.X = 0;
	//newmodel.angle.Y = 90;
	//newmodel.angle.Z = 0;
	//scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	
	//dok.init(newmodel.position.X,newmodel.position.Y,newmodel.position.Z,
	//	newmodel.angle.X,newmodel.angle.Y,newmodel.angle.Z,2,0);
	//doky.push_back(dok);

	//newmodel.position.X = 1662.0f;
	//newmodel.position.Z = 2376.0f;
	//newmodel.position.Y = -10;
	//newmodel.angle.X = 0;
	//newmodel.angle.Y = 90;
	//newmodel.angle.Z = 0;
	//scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	
	//dok.init(newmodel.position.X,newmodel.position.Y,newmodel.position.Z,
	//	newmodel.angle.X,newmodel.angle.Y,newmodel.angle.Z,3,0);
	//doky.push_back(dok);

	//newmodel.position.X = 2768.0f;
	//newmodel.position.Z = 7641.0f;
	//newmodel.position.Y = -10;
	//newmodel.angle.X = 0;
	//newmodel.angle.Y = 0;
	//newmodel.angle.Z = 0;
	//scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	
	//dok.init(newmodel.position.X,newmodel.position.Y,newmodel.position.Z,
	//	newmodel.angle.X,newmodel.angle.Y,newmodel.angle.Z,4,0);
	//doky.push_back(dok);

	//newmodel.position.X = 6435.0f;
	//newmodel.position.Z = 3276.0f;
	//newmodel.position.Y = -10;
	//newmodel.angle.X = 0;
	//newmodel.angle.Y = 90;
	//newmodel.angle.Z = 0;
	//scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);
	//dok.init(newmodel.position.X,newmodel.position.Y,newmodel.position.Z,
	//	newmodel.angle.X,newmodel.angle.Y,newmodel.angle.Z,6,0);
	//doky.push_back(dok);

	//newmodel.position.X = 5401.0f;
	//newmodel.position.Z = 6649.0f;
	//newmodel.position.Y = -10;
	//newmodel.angle.X = 0;
	//newmodel.angle.Y = 0;
	//newmodel.angle.Z = 0;
	//scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	
	//dok.init(newmodel.position.X,newmodel.position.Y,newmodel.position.Z,
	//	newmodel.angle.X,newmodel.angle.Y,newmodel.angle.Z,8,0);
	//doky.push_back(dok);

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\molo2.ASE",1,0);

	//newmodel.position.X = 6303.0f;
	//newmodel.position.Z = 1454.0f;
	//newmodel.position.Y = -18;
	//newmodel.angle.X = 0;
	//newmodel.angle.Y = 90;
	//newmodel.angle.Z = 0;
	//scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	
	//dok.init(newmodel.position.X,newmodel.position.Y,newmodel.position.Z,
	//	newmodel.angle.X,newmodel.angle.Y,newmodel.angle.Z,2,2);
	//doky.push_back(dok);

	//newmodel.position.X = 1736.0f;
	//newmodel.position.Z = 2390.0f;
	//newmodel.position.Y = -18;
	//newmodel.angle.X = 0;
	//newmodel.angle.Y = 270;
	//newmodel.angle.Z = 0;
	//scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	
	//dok.init(newmodel.position.X,newmodel.position.Y,newmodel.position.Z,
	//	newmodel.angle.X,newmodel.angle.Y,newmodel.angle.Z,3,2);
	//doky.push_back(dok);
	//
	//newmodel.position.X = 2754.0f;
	//newmodel.position.Z = 7578.0f;
	//newmodel.position.Y = -18;
	//newmodel.angle.X = 0;
	//newmodel.angle.Y = 0;
	//newmodel.angle.Z = 0;
	//scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	
	//dok.init(newmodel.position.X,newmodel.position.Y,newmodel.position.Z,
	//	newmodel.angle.X,newmodel.angle.Y,newmodel.angle.Z,4,2);
	//doky.push_back(dok);

	//newmodel.position.X = 6350.0f;
	//newmodel.position.Z = 3290.0f;
	//newmodel.position.Y = -18;
	//newmodel.angle.X = 0;
	//newmodel.angle.Y = 90;
	//newmodel.angle.Z = 0;
	//scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);
	//dok.init(newmodel.position.X,newmodel.position.Y,newmodel.position.Z,
	//	newmodel.angle.X,newmodel.angle.Y,newmodel.angle.Z,6,2);
	//doky.push_back(dok);

	//newmodel.position.X = 5387.0f;
	//newmodel.position.Z = 6574.0f;
	//newmodel.position.Y = -18;
	//newmodel.angle.X = 0;
	//newmodel.angle.Y = 0;
	//newmodel.angle.Z = 0;
	//scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	
	//dok.init(newmodel.position.X,newmodel.position.Y,newmodel.position.Z,
	//	newmodel.angle.X,newmodel.angle.Y,newmodel.angle.Z,8,2);
	//doky.push_back(dok);
	//

	//// konec nacitani dat ze souboru

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\bar.ASE",0.7,0);
	//newmodel.position.X = 4345.0f;
	//newmodel.position.Z = 445.0f;
	//newmodel.position.Y = teren.height(newmodel.position.X,newmodel.position.Z)-2;
	//newmodel.angle.X = 0;
	//newmodel.angle.Y = -120;
	//newmodel.angle.Z = 0;
	//scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	

	//newmodel.position.X = 7372.0f;
	//newmodel.position.Z = 4000.0f;
	//newmodel.position.Y = teren.height(newmodel.position.X,newmodel.position.Z)-2;
	//newmodel.angle.X = 0;
	//newmodel.angle.Y = 0;
	//newmodel.angle.Z = 0;
	//scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\bojka.ASE",1,0);
	//newmodel.position.X = 5100.0f;
	//newmodel.position.Z = 3161.0f;
	//newmodel.position.Y = 0;
	//newmodel.angle.X = 0;
	//newmodel.angle.Y = 0;
	//newmodel.angle.Z = 0;
	//scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	
	//dok.init(newmodel.position.X,newmodel.position.Y,newmodel.position.Z,
	//	newmodel.angle.X,newmodel.angle.Y,newmodel.angle.Z,5,1);
	//doky.push_back(dok);

	//newmodel.position.X = 900.0f;
	//newmodel.position.Z = 5850.0f;
	//newmodel.position.Y = 0;
	//newmodel.angle.X = 0;
	//newmodel.angle.Y = 0;
	//newmodel.angle.Z = 0;
	//scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	
	//dok.init(newmodel.position.X,newmodel.position.Y,newmodel.position.Z,
	//	newmodel.angle.X,newmodel.angle.Y,newmodel.angle.Z,7,1);
	//doky.push_back(dok);

	//newmodel.position.X = 7235.0f;
	//newmodel.position.Z = 7126.0f;
	//newmodel.position.Y = 0;
	//newmodel.angle.X = 0;
	//newmodel.angle.Y = 0;
	//newmodel.angle.Z = 0;
	//scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	
	//dok.init(newmodel.position.X,newmodel.position.Y,newmodel.position.Z,
	//	newmodel.angle.X,newmodel.angle.Y,newmodel.angle.Z,9,1);
	//doky.push_back(dok);

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\skaly.ASE",0.3,0);
	//newmodel.position.X = 900.0f;
	//newmodel.position.Z = 5850.0f;
	//newmodel.position.Y = -40;
	//newmodel.angle.X = 0;
	//newmodel.angle.Y = 0;
	//newmodel.angle.Z = 0;
	//scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\vrak.ASE",1,0);
	//newmodel.position.X = 5100.0f;
	//newmodel.position.Z = 3161.0f;
	//newmodel.position.Y = teren.height(newmodel.position.X,newmodel.position.Z)+3;
	//newmodel.angle.X = 0;
	//newmodel.angle.Y = 15;
	//newmodel.angle.Z = 45;
	//scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\bedna.ASE",0.5,0);
	//newmodel.position.X = 5090.0f;
	//newmodel.position.Z = 3155.0f;
	//newmodel.position.Y = teren.height(newmodel.position.X,newmodel.position.Z);
	//newmodel.angle.X = 0;
	//newmodel.angle.Y = 0;
	//newmodel.angle.Z = 0;
	//scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	

	//newmodel.position.X = 5085.0f;
	//newmodel.position.Z = 3158.0f;
	//newmodel.position.Y = teren.height(newmodel.position.X,newmodel.position.Z);
	//newmodel.angle.X = 0;
	//newmodel.angle.Y = 30;
	//newmodel.angle.Z = 0;
	//scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\kamen3.ASE",1,0);
	//newmodel.position.X = 5070.0f;
	//newmodel.position.Z = 3161.0f;
	//newmodel.position.Y = teren.height(newmodel.position.X,newmodel.position.Z);
	//newmodel.angle.X = 0;
	//newmodel.angle.Y = 0;
	//newmodel.angle.Z = 0;
	//scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	

	//newmodel.position.X = 5080.0f;
	//newmodel.position.Z = 3131.0f;
	//newmodel.position.Y = teren.height(newmodel.position.X,newmodel.position.Z);
	//newmodel.angle.X = 0;
	//newmodel.angle.Y = 0;
	//newmodel.angle.Z = 0;
	//scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	

	//newmodel.position.X = 5130.0f;
	//newmodel.position.Z = 3181.0f;
	//newmodel.position.Y = teren.height(newmodel.position.X,newmodel.position.Z);
	//newmodel.angle.X = 0;
	//newmodel.angle.Y = 0;
	//newmodel.angle.Z = 0;
	//scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\ho_229.ASE",3,0);
	//newmodel.position.X = 0.0f;
	//newmodel.position.Z = 0.0f;
	//newmodel.position.Y = 0.0f;
	//modely.push_back(newmodel);

	//horten.pozice.X = 4300;
	//horten.pozice.Y = 70;
	//horten.pozice.Z = 820;

	//newmodel.position.X = 7235.0f;
	//newmodel.position.Z = 7126.0f;
	//newmodel.position.Y = -10;
	//newmodel.angle.X = 0;
	//newmodel.angle.Y = 15;
	//newmodel.angle.Z = 0;
	//scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);


	//newmodel.reset();
	//newmodel.loadASE("data\\models\\powerboat.ASE",1,0);
	//newmodel.loadASE("data\\models\\powerboat.ASE",1,1);
	//newmodel.position.X = 0.0f;
	//newmodel.position.Z = 0.0f;
	////newmodel.position.y = teren.height(newmodel.position.x,newmodel.position.z);
	//newmodel.position.Y = 0.0f;
	//modely.push_back(newmodel);

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\powerboat.ASE",3,0);
	//newmodel.loadASE("data\\models\\powerboat.ASE",3,1);
	//newmodel.position.X = 0.0f;
	//newmodel.position.Z = 0.0f;
	////newmodel.position.y = teren.height(newmodel.position.x,newmodel.position.z);
	//newmodel.position.Y = 0.0f;
	//modely.push_back(newmodel);


	//newmodel.loadASE("data\\models\\powerboat.ASE",1,0);
	//unsigned int xx = 5;
	//

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\majak.ASE",3,0);
	//
	//newmodel.position.X = 5376.0f;
	//newmodel.position.Z = 1920.0f;
	//newmodel.position.Y = teren.height(newmodel.position.X,newmodel.position.Z)-20;
	//scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);
	//newmodel.angle.Y = 180;

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\skaly.ASE",3,0);
	//
	//newmodel.position.X = 4288.0f;
	//newmodel.position.Z = 4800.0f;
	//newmodel.position.Y = teren.height(newmodel.position.X,newmodel.position.Z)-20;
	//scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);
	//newmodel.angle.Y = 0;

	//jachta.init();
	//jachta.pozice.X = 4270;
	//jachta.pozice.Z = 1080;
	//jachta.pozice.Y = 5;
	//jachta.uhel.Y = 200;
	//jachta.model = 1;

	//jachta_menu.init();
	//jachta_menu.pozice.X = 4350;
	//jachta_menu.pozice.Z = 1186;
	//jachta_menu.pozice.Y = 1;
	//jachta_menu.uhel.Y = 60;
	//jachta_menu.model = 2;

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\palma_max.ASE",0.7,0);
	//newmodel.loadASE("data\\models\\palma_LOD.ASE",0.7,1);
	//
	//xx = 5000;
	//while (xx > 0)
	//{
	//	newmodel.position.X = (float)(rand())/4;
	//	newmodel.position.Z = (float)(rand())/4;
	//	if ((newmodel.position.Y = teren.height(newmodel.position.X,newmodel.position.Z)) > 15)
	//	{
	//		scena[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	
	//		xx--;
	//	}
	//}

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\palma_max.ASE",0.7,0);
	//newmodel.loadASE("data\\models\\palma2_LOD.ASE",0.7,1);
	//
	//xx = 5000;
	//while (xx > 0)
	//{
	//	newmodel.position.X = (float)(rand())/4;
	//	newmodel.position.Z = (float)(rand())/4;
	//	if ((newmodel.position.Y = teren.height(newmodel.position.X,newmodel.position.Z)) > 15)
	//	{
	//		scena[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	
	//		xx--;
	//	}
	//}

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\kytka1.ASE",1,0);
	//
	//xx=1000; 
	//while (xx > 0)
	//{
	//	newmodel.position.X = (float)(rand())/4;
	//	newmodel.position.Z = (float)(rand())/4;
	//	if ((newmodel.position.Y = teren.height(newmodel.position.X,newmodel.position.Z)) > 15)
	//	{
	//		scena[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	
	//		xx--;
	//	}
	//}

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\kytka2.ASE",1,0);
	//
	//xx=1000; 
	//while (xx > 0)
	//{
	//	newmodel.position.X = (float)(rand())/4;
	//	newmodel.position.Z = (float)(rand())/4;
	//	if ((newmodel.position.Y = teren.height(newmodel.position.X,newmodel.position.Z)) > 15)
	//	{
	//		scena[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	
	//		xx--;
	//	}
	//}

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\kytka3.ASE",1,0);
	//
	//xx=1000; 
	//while (xx > 0)
	//{
	//	newmodel.position.X = (float)(rand())/4;
	//	newmodel.position.Z = (float)(rand())/4;
	//	if ((newmodel.position.Y = teren.height(newmodel.position.X,newmodel.position.Z)) > 15)
	//	{
	//		scena[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	
	//		xx--;
	//	}
	//}

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\kytka4.ASE",1,0);
	//
	//xx=1000; 
	//while (xx > 0)
	//{
	//	newmodel.position.X = (float)(rand())/4;
	//	newmodel.position.Z = (float)(rand())/4;
	//	if ((newmodel.position.Y = teren.height(newmodel.position.X,newmodel.position.Z)) > 15)
	//	{
	//		scena[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	
	//		xx--;
	//	}
	//}

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\kytka5.ASE",1,0);
	//
	//xx=1000; 
	//while (xx > 0)
	//{
	//	newmodel.position.X = (float)(rand())/4;
	//	newmodel.position.Z = (float)(rand())/4;
	//	if ((newmodel.position.Y = teren.height(newmodel.position.X,newmodel.position.Z)) > 15)
	//	{
	//		scena[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	
	//		xx--;
	//	}
	//}

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\kytka6.ASE",1,0);
	//
	//xx=1000; 
	//while (xx > 0)
	//{
	//	newmodel.position.X = (float)(rand())/4;
	//	newmodel.position.Z = (float)(rand())/4;
	//	if ((newmodel.position.Y = teren.height(newmodel.position.X,newmodel.position.Z)) > 15)
	//	{
	//		scena[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	
	//		xx--;
	//	}
	//}

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\kamen2.ASE",1,0);
	//
	//xx = 20;
	//while (xx > 0)
	//{
	//	newmodel.position.X = (float)(rand())/4;
	//	newmodel.position.Z = (float)(rand())/4;
	//	if ((newmodel.position.Y = teren.height(newmodel.position.X,newmodel.position.Z)) < 15 &&
	//		(newmodel.position.Y = teren.height(newmodel.position.X,newmodel.position.Z)) > -2)
	//	{
	//		scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	
	//		xx--;
	//	}
	//}

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\kamen.ASE",1,0);
	//
	//xx = 20;
	//while (xx > 0)
	//{
	//	newmodel.position.X = (float)(rand())/4;
	//	newmodel.position.Z = (float)(rand())/4;
	//	if ((newmodel.position.Y = teren.height(newmodel.position.X,newmodel.position.Z)) < 15 &&
	//		(newmodel.position.Y = teren.height(newmodel.position.X,newmodel.position.Z)) > -2)
	//	{
	//		scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	
	//		xx--;
	//	}
	//}

	///*newmodel.reset();
	//newmodel.loadASE("data\\models\\trava.ASE",1,0);
	//newmodel.loadASE("data\\models\\trava_LOD.ASE",1,1);
	//xx = 1000;
	//while (xx > 0)
	//{
	//	newmodel.position.x = (float)(rand())/4;
	//	newmodel.position.z = (float)(rand())/4;
	//	if ((newmodel.position.y = teren.height(newmodel.position.x,newmodel.position.z)) < -2)
	//	{
	//		scena[teren.quad(newmodel.position.x,newmodel.position.z)].object.push_back(newmodel);	
	//		xx--;
	//	}
	//}*/

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\kamen3.ASE",1,0);
	//
	//xx = 10;
	//while (xx > 0)
	//{
	//	newmodel.position.X = (float)(rand())/4;
	//	newmodel.position.Z = (float)(rand())/4;
	//	if ((newmodel.position.Y = teren.height(newmodel.position.X,newmodel.position.Z)) < 15 &&
	//		(newmodel.position.Y = teren.height(newmodel.position.X,newmodel.position.Z)) > -2)
	//	{
	//		scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	
	//		xx--;
	//	}
	//}

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\kamen4.ASE",1,0);
	//
	//xx = 10;
	//while (xx > 0)
	//{
	//	newmodel.position.X = (float)(rand())/4;
	//	newmodel.position.Z = (float)(rand())/4;
	//	if ((newmodel.position.Y = teren.height(newmodel.position.X,newmodel.position.Z)) < 15 &&
	//		(newmodel.position.Y = teren.height(newmodel.position.X,newmodel.position.Z)) > -2)
	//	{
	//		scena_stat[teren.quad(newmodel.position.X,newmodel.position.Z)].object.push_back(newmodel);	
	//		xx--;
	//	}
	//}
/*
	newmodel.reset();
	newmodel.loadASE("data\\models\\kytka3.ASE");
	
	for (unsigned int xx=0; xx<2000; xx++)
	{
		newmodel.position.x = (float)(rand())/4;
		newmodel.position.z = (float)(rand())/4;
		if ((newmodel.position.y = teren.height(newmodel.position.x,newmodel.position.z)) > 10)
			scena[teren.quad(newmodel.position.x,newmodel.position.z)].object.push_back(newmodel);	
	}

	newmodel.reset();
	newmodel.loadASE("data\\models\\kytka4.ASE");
	
	for (unsigned int xx=0; xx<2000; xx++)
	{
		newmodel.position.x = (float)(rand())/4;
		newmodel.position.z = (float)(rand())/4;
		if ((newmodel.position.y = teren.height(newmodel.position.x,newmodel.position.z)) > 10)
			scena[teren.quad(newmodel.position.x,newmodel.position.z)].object.push_back(newmodel);	
	}

	newmodel.reset();
	newmodel.loadASE("data\\models\\kytka5.ASE");
	
	for (unsigned int xx=0; xx<2000; xx++)
	{
		newmodel.position.x = (float)(rand())/4;
		newmodel.position.z = (float)(rand())/4;
		if ((newmodel.position.y = teren.height(newmodel.position.x,newmodel.position.z)) > 10)
			scena[teren.quad(newmodel.position.x,newmodel.position.z)].object.push_back(newmodel);	
	}

	newmodel.reset();
	newmodel.loadASE("data\\models\\kytka6.ASE");
	
	for (unsigned int xx=0; xx<2000; xx++)
	{
		newmodel.position.x = (float)(rand())/4;
		newmodel.position.z = (float)(rand())/4;
		if ((newmodel.position.y = teren.height(newmodel.position.x,newmodel.position.z)) > 10)
			scena[teren.quad(newmodel.position.x,newmodel.position.z)].object.push_back(newmodel);	
	}
/*
	newmodel.reset();
	newmodel.loadASE("data\\models\\kamen1.ASE");
	
	for (unsigned int xx=0; xx<200; xx++)
	{
		newmodel.position.x = (float)(rand())/9;
		newmodel.position.z = (float)(rand())/9;
		if ((newmodel.position.y = teren.height(newmodel.position.x,newmodel.position.z)) > 0)
			scena.push_back(newmodel);
	}

	newmodel.reset();
	newmodel.loadASE("data\\models\\kamen2.ASE");
	
	for (unsigned int xx=0; xx<200; xx++)
	{
		newmodel.position.x = (float)(rand())/9;
		newmodel.position.z = (float)(rand())/9;
		if ((newmodel.position.y = teren.height(newmodel.position.x,newmodel.position.z)) > 0)
			scena.push_back(newmodel);
	}

	newmodel.reset();
	newmodel.loadASE("data\\models\\kamen3.ASE");
	
	for (unsigned int xx=0; xx<200; xx++)
	{
		newmodel.position.x = (float)(rand())/9;
		newmodel.position.z = (float)(rand())/9;
		if ((newmodel.position.y = teren.height(newmodel.position.x,newmodel.position.z)) > 0)
			scena.push_back(newmodel);
	}

	newmodel.reset();
	newmodel.loadASE("data\\models\\kamen4.ASE");
	
	for (unsigned int xx=0; xx<200; xx++)
	{
		newmodel.position.x = (float)(rand())/9;
		newmodel.position.z = (float)(rand())/9;
		if ((newmodel.position.y = teren.height(newmodel.position.x,newmodel.position.z)) > 0)
			scena.push_back(newmodel);
	}
*/
	/*pozice_x = 2300;
	pozice_z = 1800;
	pozice_y = terrain.height(pozice_x,pozice_z);*/
    //pozice_y = 170;

	//glVertexPointer(3, GL_FLOAT, 0, vertexs);	// nahrati vrcholu do vertex array
	//glNormalPointer(GL_FLOAT, 0, normals);	// nahrati normal do vertex array
	//glClientActiveTextureARB(GL_TEXTURE0_ARB);	// prvni texturovaci jednotka
	//glTexCoordPointer (2, GL_FLOAT, 0, tex_cor);

	//glClientActiveTextureARB(GL_TEXTURE1_ARB);	// druha texturovaci jednotka
	//glEnableClientState(GL_TEXTURE_COORD_ARRAY);	// pole texturovych souradnic
	//glTexCoordPointer (2, GL_FLOAT, 0, tex_cor1);

    //glEnable(GL_TEXTURE_2D);// Zapne texturove mapovani

	//slunce
	/*GLfloat no_mat[] = { 0.0, 0.0, 0.0, 1.0 };
	GLfloat mat_ambient[] = { 0.3, 0.3, 0.3, 1.0 };
	GLfloat mat_diffuse[] = { 0.5, 0.5, 0.5, 1.0 };
	GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat no_shininess[] = { 0.0 };
	GLfloat low_shininess[] = { 5.0 };
	GLfloat high_shininess[] = { 100.0 };
	GLfloat mat_emission[] = {0.3, 0.2, 0.2, 0.0};*/

	//glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
	//glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
	//glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);


//glMaterialfv(GL_BACK, GL_AMBIENT, mat_ambient);
//glMaterialfv(GL_BACK, GL_DIFFUSE, mat_diffuse);
//glMaterialfv(GL_BACK, GL_SPECULAR, mat_specular);


	//float lt_ambient[]= {0.2, 0.2, 0.2, 0.5};       /* ambientni slozka svetla */
	//float lt_diffuse[]= {0.5, 0.5, 0.5, 1.0};       /* difuzni slozka svetla */
	//float lt_specular[]={1.0, 1.0, 1.0, 1.0};       /* barva odlesku */
	//float lt_position[]={5000.0, 10000.0, 2500.0, 0.0};       /* definice pozice svetla */
	//float lt_constant[]={0.07};       /* definice pozice svetla */
	//float lt_lin[]={0.000000005};       /* definice pozice svetla */

	//glLightfv(GL_LIGHT0,GL_AMBIENT,lt_ambient); /* nastaveni parametru svetla */
 //   glLightfv(GL_LIGHT0,GL_DIFFUSE,lt_diffuse);
 //   glLightfv(GL_LIGHT0,GL_SPECULAR,lt_specular);
 //   glLightfv(GL_LIGHT0,GL_POSITION,lt_position);
	//glLightfv(GL_LIGHT0,GL_CONSTANT_ATTENUATION,lt_constant);
	//glLightfv(GL_LIGHT0,GL_LINEAR_ATTENUATION,lt_lin);

	//glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);

	//glEnable(GL_LIGHTING);                      /* povoleni osvetleni */
 //   glEnable(GL_LIGHT0);                        /* povoleni prvniho svetla */

	////teren.Update(0);
	//play = false;
 //
	////konec slunce

	//kredity.init();

	//FSOUND_PlaySound(0, samp1);

	return true; 
}

void CreateNewDock(Terrain& terrain, Vector3f position, Vector3f rotation, int id)
{
	ModelInstance newModelInstance(Model::GetModel("data\\models\\molo.obj", ""));
	newModelInstance.Position = position;
	newModelInstance.Rotation = rotation;
	newModelInstance.Scale = Vector3f(2, 2, 2);

	terrain.Docks.push_back(Molo(newModelInstance.Position, newModelInstance.Rotation, id, 1));

	terrain.Objects[terrain.quad(newModelInstance.Position.X, newModelInstance.Position.Z)].push_back(std::move(newModelInstance));
}

void CreateNewDock2(Terrain& terrain, Vector3f position, Vector3f rotation, int id)
{
	ModelInstance newModelInstance(Model::GetModel("data\\models\\molo2.obj", ""));
	newModelInstance.Position = position;
	newModelInstance.Rotation = rotation;
	newModelInstance.Scale = Vector3f(1, 1, 1);	

	terrain.Docks.push_back(Molo(newModelInstance.Position, newModelInstance.Rotation, id, 1));

	terrain.Objects[terrain.quad(newModelInstance.Position.X, newModelInstance.Position.Z)].push_back(std::move(newModelInstance));
}

void CreateBar(Terrain& terrain, Vector3f position, Vector3f rotation)
{
	ModelInstance newModelInstance(Model::GetModel("data\\models\\bar.obj", ""));
	newModelInstance.Position = Vector3f(position.X, terrain.GetHeight(position) - 2, position.Z);
	newModelInstance.Rotation = rotation;
	newModelInstance.Scale = Vector3f(0.7f, 0.7f, 0.7f);

	terrain.Objects[terrain.quad(newModelInstance.Position.X, newModelInstance.Position.Z)].push_back(std::move(newModelInstance));
}

void CreateBuoy(Terrain& terrain, Vector3f position, Vector3f rotation, int id)
{
	ModelInstance newModelInstance(Model::GetModel("data\\models\\bojka.obj", ""));
	newModelInstance.Position = position;
	newModelInstance.Rotation = rotation;
	newModelInstance.Scale = Vector3f(1, 1, 1);	

	terrain.Docks.push_back(std::move(Molo(newModelInstance.Position, newModelInstance.Rotation, id, 1)));

	terrain.Objects[terrain.quad(newModelInstance.Position.X, newModelInstance.Position.Z)].push_back(std::move(newModelInstance));
}

void CreateRock(Terrain& terrain, Vector3f position, Vector3f rotation, float scale, bool attachTerrain)
{
	ModelInstance newModelInstance(Model::GetModel("data\\models\\skaly.obj", ""));
	newModelInstance.Position = position;
	newModelInstance.Rotation = rotation;
	newModelInstance.Scale = Vector3f(scale, scale, scale);

	if (attachTerrain)
	{
		newModelInstance.Position.Y += terrain.GetHeight(position);
	}

	terrain.Objects[terrain.quad(newModelInstance.Position.X, newModelInstance.Position.Z)].push_back(std::move(newModelInstance));
}

void CreateObject(Terrain& terrain, const std::string& modelFilename, Vector3f position, Vector3f rotation, float scale, bool attachTerrain)
{
	ModelInstance newModelInstance(Model::GetModel(modelFilename, ""));
	newModelInstance.Position = position;
	newModelInstance.Rotation = rotation;
	newModelInstance.Scale = Vector3f(scale, scale, scale);

	if (attachTerrain)
	{
		newModelInstance.Position.Y += terrain.GetHeight(position);
	}

	terrain.Objects[terrain.quad(newModelInstance.Position.X, newModelInstance.Position.Z)].push_back(std::move(newModelInstance));
}

void GenerateObjectsOnTerrain(Terrain& terrain)
{
	CreateNewDock(terrain, Vector3f(7372.0f, -10, 3806.0f), Vector3f(0, 0, 0), 0);
	CreateNewDock(terrain, Vector3f(4345.0f, -10, 660.0f), Vector3f(0, 0, 0), 1);
	CreateNewDock(terrain, Vector3f(6378.0f, -10, 1440.0f), Vector3f(0, 90, 0), 2);
	CreateNewDock(terrain, Vector3f(1662.0f, -10, 2376.0f), Vector3f(0, 90, 0),3);
	CreateNewDock(terrain, Vector3f(2768.0f, -10, 7641.0f), Vector3f(0, 0, 0),4);
	CreateNewDock(terrain, Vector3f(6435.0f, -10, 3276.0f), Vector3f(0, 90, 0),6);
	CreateNewDock(terrain, Vector3f(5401.0f, -10, 6649.0f), Vector3f(0, 0, 0),8);

	CreateNewDock2(terrain, Vector3f(6303.0f, -18, 1454.0f), Vector3f(0, 90, 0), 2);
	CreateNewDock2(terrain, Vector3f(1736.0f, -18, 2390.0f), Vector3f(0, 270, 0),3);
	CreateNewDock2(terrain, Vector3f(2754.0f, -18, 7578.0f), Vector3f(0, 0, 0),4);
	CreateNewDock2(terrain, Vector3f(6350.0f, -18, 3290.0f), Vector3f(0, 90, 0),6);
	CreateNewDock2(terrain, Vector3f(5387.0f, -18, 6574.0f), Vector3f(0, 0, 0),8);
	
	CreateBar(terrain, Vector3f(4345.0f, 0, 445.0f) , Vector3f(0, 90, 0));
	CreateBar(terrain, Vector3f(7372.0f, 0, 4000.0f), Vector3f(0, 0, 0));
	
	CreateBuoy(terrain, Vector3f(5100.0f, 0, 3161.0f), Vector3f(0, 0, 0), 5);
	CreateBuoy(terrain, Vector3f(900.0f, 0, 5850.0f), Vector3f(0, 0, 0), 7);
	CreateBuoy(terrain, Vector3f(7235.0f, 0, 7126.0f), Vector3f(0, 0, 0), 9);
	
	CreateRock(terrain, Vector3f(900.0f, -40, 5850.0f), Vector3f(0, 0, 0), 0.3f, false);
	CreateRock(terrain, Vector3f(4288.0f, -20, 4800.0f), Vector3f(0, 0, 0), 3, true);
	/*
	CreateObject(scene, "data\\models\\vrak.ASE", Vector3f(5100.0f, 3, 3161.0f), Vector3f(0, 15, 45), 1, true);

	CreateObject(scene, "data\\models\\bedna.ASE", Vector3f(5090.0f, 0, 3155.0f), Vector3f(0, 0, 0), 0.5f, true);
	CreateObject(scene, "data\\models\\bedna.ASE", Vector3f(5085.0f, 0, 3158.0f), Vector3f(0, 30, 0), 0.5f, true);

	CreateObject(scene, "data\\models\\kamen3.ASE", Vector3f(5070.0f, 0, 3161.0f), Vector3f(0, 0, 0), 1, true);
	CreateObject(scene, "data\\models\\kamen3.ASE", Vector3f(5080.0f, 0, 3131.0f), Vector3f(0, 0, 0), 1, true);
	CreateObject(scene, "data\\models\\kamen3.ASE", Vector3f(5130.0f, 0, 3181.0f), Vector3f(0, 0, 0), 1, true);
	
	CreateObject(scene, "data\\models\\ho_229.ASE", Vector3f(7235.0f, -10, 7126.0f), Vector3f(0, 15, 0), 3, false);
	CreateObject(scene, "data\\models\\majak.ASE", Vector3f(5376.0f, -20, 1920.0f), Vector3f(0, 180, 0), 3, true);
	*/
	
	//CreateRock(scene, Vector3f(0.0f, 0, 0.0f), Vector3f(0, 0, 0), 1, false);
	
	
	
	
	
	
	//
	//
	//
	//
	//newmodel.reset();
	//newmodel.loadASE("data\\models\\powerboat.ASE", 1, 0);
	//newmodel.loadASE("data\\models\\powerboat.ASE", 1, 1);
	//newmodel.position.X = 0.0f;
	//newmodel.position.Z = 0.0f;
	////newmodel.position.y = teren.height(newmodel.position.x,newmodel.position.z);
	//newmodel.position.Y = 0.0f;
	//modely.push_back(newmodel);

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\powerboat.ASE", 3, 0);
	//newmodel.loadASE("data\\models\\powerboat.ASE", 3, 1);
	//newmodel.position.X = 0.0f;
	//newmodel.position.Z = 0.0f;
	////newmodel.position.y = teren.height(newmodel.position.x,newmodel.position.z);
	//newmodel.position.Y = 0.0f;
	//modely.push_back(newmodel);


	//newmodel.loadASE("data\\models\\powerboat.ASE", 1, 0);
	//unsigned int xx = 5;


	//

	//newmodel.reset();
	//
	//newmodel.angle.Y = 0;

	//jachta.init();
	//jachta.pozice.X = 4270;
	//jachta.pozice.Z = 1080;
	//jachta.pozice.Y = 5;
	//jachta.uhel.Y = 200;
	//jachta.model = 1;

	//jachta_menu.init();
	//jachta_menu.pozice.X = 4350;
	//jachta_menu.pozice.Z = 1186;
	//jachta_menu.pozice.Y = 1;
	//jachta_menu.uhel.Y = 60;
	//jachta_menu.model = 2;

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\palma_max.ASE", 0.7, 0);
	//newmodel.loadASE("data\\models\\palma_LOD.ASE", 0.7, 1);

	//xx = 5000;
	//while (xx > 0)
	//{
	//	newmodel.position.X = (float)(rand()) / 4;
	//	newmodel.position.Z = (float)(rand()) / 4;
	//	if ((newmodel.position.Y = terrain.height(newmodel.position.X, newmodel.position.Z)) > 15)
	//	{
	//		scena[terrain.quad(newmodel.position.X, newmodel.position.Z)].object.push_back(newmodel);
	//		xx--;
	//	}
	//}

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\palma_max.ASE", 0.7, 0);
	//newmodel.loadASE("data\\models\\palma2_LOD.ASE", 0.7, 1);

	//xx = 5000;
	//while (xx > 0)
	//{
	//	newmodel.position.X = (float)(rand()) / 4;
	//	newmodel.position.Z = (float)(rand()) / 4;
	//	if ((newmodel.position.Y = terrain.height(newmodel.position.X, newmodel.position.Z)) > 15)
	//	{
	//		scena[terrain.quad(newmodel.position.X, newmodel.position.Z)].object.push_back(newmodel);
	//		xx--;
	//	}
	//}

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\kytka1.ASE", 1, 0);

	//xx = 1000;
	//while (xx > 0)
	//{
	//	newmodel.position.X = (float)(rand()) / 4;
	//	newmodel.position.Z = (float)(rand()) / 4;
	//	if ((newmodel.position.Y = terrain.height(newmodel.position.X, newmodel.position.Z)) > 15)
	//	{
	//		scena[terrain.quad(newmodel.position.X, newmodel.position.Z)].object.push_back(newmodel);
	//		xx--;
	//	}
	//}

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\kytka2.ASE", 1, 0);

	//xx = 1000;
	//while (xx > 0)
	//{
	//	newmodel.position.X = (float)(rand()) / 4;
	//	newmodel.position.Z = (float)(rand()) / 4;
	//	if ((newmodel.position.Y = terrain.height(newmodel.position.X, newmodel.position.Z)) > 15)
	//	{
	//		scena[terrain.quad(newmodel.position.X, newmodel.position.Z)].object.push_back(newmodel);
	//		xx--;
	//	}
	//}

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\kytka3.ASE", 1, 0);

	//xx = 1000;
	//while (xx > 0)
	//{
	//	newmodel.position.X = (float)(rand()) / 4;
	//	newmodel.position.Z = (float)(rand()) / 4;
	//	if ((newmodel.position.Y = terrain.height(newmodel.position.X, newmodel.position.Z)) > 15)
	//	{
	//		scena[terrain.quad(newmodel.position.X, newmodel.position.Z)].object.push_back(newmodel);
	//		xx--;
	//	}
	//}

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\kytka4.ASE", 1, 0);

	//xx = 1000;
	//while (xx > 0)
	//{
	//	newmodel.position.X = (float)(rand()) / 4;
	//	newmodel.position.Z = (float)(rand()) / 4;
	//	if ((newmodel.position.Y = terrain.height(newmodel.position.X, newmodel.position.Z)) > 15)
	//	{
	//		scena[terrain.quad(newmodel.position.X, newmodel.position.Z)].object.push_back(newmodel);
	//		xx--;
	//	}
	//}

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\kytka5.ASE", 1, 0);

	//xx = 1000;
	//while (xx > 0)
	//{
	//	newmodel.position.X = (float)(rand()) / 4;
	//	newmodel.position.Z = (float)(rand()) / 4;
	//	if ((newmodel.position.Y = terrain.height(newmodel.position.X, newmodel.position.Z)) > 15)
	//	{
	//		scena[terrain.quad(newmodel.position.X, newmodel.position.Z)].object.push_back(newmodel);
	//		xx--;
	//	}
	//}

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\kytka6.ASE", 1, 0);

	//xx = 1000;
	//while (xx > 0)
	//{
	//	newmodel.position.X = (float)(rand()) / 4;
	//	newmodel.position.Z = (float)(rand()) / 4;
	//	if ((newmodel.position.Y = terrain.height(newmodel.position.X, newmodel.position.Z)) > 15)
	//	{
	//		scena[terrain.quad(newmodel.position.X, newmodel.position.Z)].object.push_back(newmodel);
	//		xx--;
	//	}
	//}

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\kamen2.ASE", 1, 0);

	//xx = 20;
	//while (xx > 0)
	//{
	//	newmodel.position.X = (float)(rand()) / 4;
	//	newmodel.position.Z = (float)(rand()) / 4;
	//	if ((newmodel.position.Y = terrain.height(newmodel.position.X, newmodel.position.Z)) < 15 &&
	//		(newmodel.position.Y = terrain.height(newmodel.position.X, newmodel.position.Z)) > -2)
	//	{
	//		scena_stat[terrain.quad(newmodel.position.X, newmodel.position.Z)].object.push_back(newmodel);
	//		xx--;
	//	}
	//}

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\kamen.ASE", 1, 0);

	//xx = 20;
	//while (xx > 0)
	//{
	//	newmodel.position.X = (float)(rand()) / 4;
	//	newmodel.position.Z = (float)(rand()) / 4;
	//	if ((newmodel.position.Y = terrain.height(newmodel.position.X, newmodel.position.Z)) < 15 &&
	//		(newmodel.position.Y = terrain.height(newmodel.position.X, newmodel.position.Z)) > -2)
	//	{
	//		scena_stat[terrain.quad(newmodel.position.X, newmodel.position.Z)].object.push_back(newmodel);
	//		xx--;
	//	}
	//}

	///*newmodel.reset();
	//newmodel.loadASE("data\\models\\trava.ASE",1,0);
	//newmodel.loadASE("data\\models\\trava_LOD.ASE",1,1);
	//xx = 1000;
	//while (xx > 0)
	//{
	//	newmodel.position.x = (float)(rand())/4;
	//	newmodel.position.z = (float)(rand())/4;
	//	if ((newmodel.position.y = teren.height(newmodel.position.x,newmodel.position.z)) < -2)
	//	{
	//		scena[teren.quad(newmodel.position.x,newmodel.position.z)].object.push_back(newmodel);
	//		xx--;
	//	}
	//}*/

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\kamen3.ASE", 1, 0);

	//xx = 10;
	//while (xx > 0)
	//{
	//	newmodel.position.X = (float)(rand()) / 4;
	//	newmodel.position.Z = (float)(rand()) / 4;
	//	if ((newmodel.position.Y = terrain.height(newmodel.position.X, newmodel.position.Z)) < 15 &&
	//		(newmodel.position.Y = terrain.height(newmodel.position.X, newmodel.position.Z)) > -2)
	//	{
	//		scena_stat[terrain.quad(newmodel.position.X, newmodel.position.Z)].object.push_back(newmodel);
	//		xx--;
	//	}
	//}

	//newmodel.reset();
	//newmodel.loadASE("data\\models\\kamen4.ASE", 1, 0);

	//xx = 10;
	//while (xx > 0)
	//{
	//	newmodel.position.X = (float)(rand()) / 4;
	//	newmodel.position.Z = (float)(rand()) / 4;
	//	if ((newmodel.position.Y = terrain.height(newmodel.position.X, newmodel.position.Z)) < 15 &&
	//		(newmodel.position.Y = terrain.height(newmodel.position.X, newmodel.position.Z)) > -2)
	//	{
	//		scena_stat[terrain.quad(newmodel.position.X, newmodel.position.Z)].object.push_back(newmodel);
	//		xx--;
	//	}
	//}
}