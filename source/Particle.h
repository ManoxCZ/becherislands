/**************************************************************/
/* File : Max_Particle.h									  */
/* Developed :  Manox (manox@atlas.cz)						  */
/* Date : 8.6.2005											  */
/**************************************************************/

#ifndef _PARTICLE_H
#define _PARTICLE_H

#include "Color.h"
#include "Vector.h"
#include "Texture.h"


class Particle
{
private:
	Vector3f _position;	
	Vector3f _velocity;		
	float _totalLifetime;
	float _lifetime;	
	Color _startColor;
	Color _endColor;
	float _startSize;	
	float _endSize;			

public:
	Particle(float totalLifetime, 
		const Color& startColor, const Color& endColor,
		float startSize, float endSize);

	void Render() const;	
	void Update(float deltaTime);
	bool IsAlive() const;
	void Spawn(const Vector3f& position, const Vector3f& velocity);
};

#endif
