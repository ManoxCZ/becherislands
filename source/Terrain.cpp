/**************************************************************/
/* File : Max_Terrain.cpp									  */
/* Developed : Manox (manox@atlas.cz)						  */
/* Date : 2.6.2005											  */
/**************************************************************/

#include "Terrain.h"
#include "Math.h"
#include "Init.h"
#include <windows.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include <gl\glut.h>
#include "stb_image.h"


const float Terrain::WATER_LEVEL = 90.12f;
const float Terrain::water_sirka = 50000.0f;
const float Terrain::water_stretch = 3000.0f;
const float Terrain::CELL_SIZE = 64.0f;
const float Terrain::VERTICAL_SCALE = 2.0f;

Terrain::Terrain()
	:
	_wavesTotalTime(0),
	_waterTexture(Texture::GetTexture("data/textures/water.bmp"))
{ 	
	
};


bool Terrain::inside_triangle(float px, float py)
{
	/*float Y = (py-bod_A_y - ((px-bod_A_x-Y*(bod_C_x-bod_A_x))/(bod_B_x-bod_A_x))*(bod_B_y-bod_A_y))/(bod_B_y-bod_A_y);
	float X = (px-bod_A_x-Y*(bod_C_x-bod_A_x))/(bod_B_x-bod_A_x);

	if (X>=0 && Y>=0 && X+Y<=1)*/
	
	float a1 = px - bod_A_x;
	float b1 = py - bod_A_y;
	float a2 = px - bod_B_x;
	float b2 = py - bod_B_y;
 
	float t1 = ((-b1*bod_B_x + a1*bod_B_y - (-b1*px + a1*py))/
		(b1*(bod_C_x-bod_B_x)-a1*(bod_C_y-bod_B_y)));
	float t2 = ((-b2*bod_C_x + a2*bod_C_y-(-b2*px + a2*py))/
		(b2*(bod_A_x-bod_C_x)-a2*(bod_A_y-bod_C_y)));
	float t3 = ((-b1*bod_B_x + a1*bod_B_y - (-b1*px + a1*py))/
		(b1*(bod_D_x-bod_B_x)-a1*(bod_D_y-bod_B_y)));
	float t4 = ((-b2*bod_D_x + a2*bod_D_y-(-b2*px + a2*py))/
		(b2*(bod_A_x-bod_D_x)-a2*(bod_A_y-bod_D_y)));
  
	if ((t1>0 && t1<1 && t2>0 && t2<1) || (t3>0 && t3<1 && t4>0 && t4<1)) return true;
	else return false;
}

void Terrain::UpdateVisibility(const Camera& camera)
{
	/*unsigned int face;
	face = (int)(jachta.pozice.x)/((int)(4*velikost_policka))+(int)(jachta.pozice.z)/((int)(4*velikost_policka))*32;
	*/

	{
		//if (play) scene->jachta.Update(deltaTime);
		/*bod_A_x = camera.Position.X - 500 * sin(DegToRad(scene->jachta.uhel.Y - uhel + 180));
		bod_A_y = camera.Position.Z - 500 * cos(DegToRad(scene->jachta.uhel.Y - uhel + 180));

		bod_B_x = camera.Position.X - 10000 * sin(DegToRad(scene->jachta.uhel.Y - uhel));
		bod_B_y = camera.Position.Z - 10000 * cos(DegToRad(scene->jachta.uhel.Y - uhel));

		bod_C_x = camera.Position.X - 500 * sin(DegToRad(scene->jachta.uhel.Y + uhel + 180));
		bod_C_y = camera.Position.Z - 500 * cos(DegToRad(scene->jachta.uhel.Y + uhel + 180));

		bod_D_x = camera.Position.X - 10000 * sin(DegToRad(scene->jachta.uhel.Y + uhel));
		bod_D_y = camera.Position.Z - 10000 * cos(DegToRad(scene->jachta.uhel.Y + uhel));*/

		_visibleQuads.clear();

		for (unsigned int i = 0; i < _quadSize * _quadSize; i++)
		{
			//if (inside_triangle(_vertices[_quadtree[i]].X,_vertices[_quadtree[i]].Z))
			/*{
				VisibleQuad newVisibleQuad;
				
				newVisibleQuad.Id = i;
				
				newVisibleQuad.Distance = sqrt((scene->jachta.pozice.X - _vertices[_quadtree[i]].X) *
					(scene->jachta.pozice.X - _vertices[_quadtree[i]].X) +
					(scene->jachta.pozice.Z - _vertices[_quadtree[i]].Z) *
					(scene->jachta.pozice.Z - _vertices[_quadtree[i]].Z));

				_visibleQuads.push_back(std::move(newVisibleQuad));
			}*/
		}
	}
}

void Terrain::Update(float deltaTime)
{	
	UpdateWater(deltaTime);
}

void Terrain::Render(const Camera& camera) const
{	
	//UpdateVisibility();

	RenderTerrain(camera);

	RenderObjects(camera);

	RenderWater(camera);	
}

void Terrain::UpdateWater(float deltaTime)
{
	_wavesTotalTime += deltaTime*5;

	for (SeaWave& wave : _beachWaves)
	{
		wave.Update(deltaTime);
	}
}

void Terrain::RenderWater(const Camera& camera) const
{
	glLoadIdentity();

	camera.SetLookAt();

	glDepthMask(false);
	glEnable(GL_BLEND);
	
	glBlendFunc(GL_SRC_COLOR, GL_DST_COLOR);	

	_waterTexture.Bind();

	glPushMatrix();

	glTranslatef(sin(_wavesTotalTime) * 3, 0, cos(_wavesTotalTime) * 2);

	glBegin(GL_QUADS);
	glTexCoord2f(0, 0);
	glVertex3f(-water_sirka, 0, -water_sirka);
	glTexCoord2f(0.0f, water_stretch);
	glVertex3f(-water_sirka, 0, water_sirka);
	glTexCoord2f(water_stretch, water_stretch);
	glVertex3f(water_sirka, 0, water_sirka);
	glTexCoord2f(water_stretch, 0.0f);
	glVertex3f(water_sirka, 0, -water_sirka);
	glEnd();
		
	glPopMatrix();

	glBlendFunc(GL_SRC_COLOR, GL_ONE_MINUS_SRC_COLOR);

	glPushMatrix();

	glTranslatef(-sin(_wavesTotalTime) * 4, 0, -cos(_wavesTotalTime) * 3);

	glBegin(GL_QUADS);
	glTexCoord2f(0, 0);
	glVertex3f(-water_sirka, 0, -water_sirka);
	glTexCoord2f(0.0f, water_stretch);
	glVertex3f(-water_sirka, 0, water_sirka);
	glTexCoord2f(water_stretch, water_stretch);
	glVertex3f(water_sirka, 0, water_sirka);
	glTexCoord2f(water_stretch, 0.0f);
	glVertex3f(water_sirka, 0, -water_sirka);
	glEnd();

	glPopMatrix();

	glDisable(GL_BLEND);
	glDepthMask(true);

	for (const SeaWave& wave : _beachWaves)
	{
		wave.Render();
	}
}

void Terrain::render_ref()
{
	glBlendFunc(GL_DST_COLOR, GL_ONE_MINUS_DST_COLOR);
	
	glDrawArrays(GL_TRIANGLES, 0, _vertices.size());
}

void Terrain::RenderTerrain(const Camera& camera) const
{	
	glLoadIdentity();	

	camera.SetLookAt();

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);

	glVertexPointer(3, GL_FLOAT, 0, _vertices.data());
	glNormalPointer(GL_FLOAT, 0, _normals.data());
	glTexCoordPointer(2, GL_FLOAT, 0, _texCoord0.data());

	_mainTexture.Bind();

	glDrawArrays(GL_TRIANGLES, 0, _vertices.size());

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
}

void Terrain::RenderObjects(const Camera& camera) const
{
	for (size_t i = 0; i < 1024; i++)
	{
		for (const ModelInstance& instance : Objects[i])
		{
			instance.Render(camera);
		}
	}
}

bool Terrain::loadData(const std::string& file, const std::string& texturka)
{
	stbi_set_flip_vertically_on_load(true);

	int numChannels;

	unsigned char* data = stbi_load(file.c_str(), &_width, &_height, &numChannels, 0);

	_mainTexture = Texture::GetTexture(texturka);
	_detailTexture = Texture::GetTexture("data/textures/detail.bmp");	

	unsigned int pocet_nad = 0;
	unsigned int pocet_pod = 0;

	for (unsigned int row = 0; row < (_height - 1); row++)
	{
		for (unsigned int col = 0; col < (_width - 1); col++)
		{
			_vertices.push_back(std::move(Vector3f(
				col * CELL_SIZE,
				data[(col + row * _width) * 3] * VERTICAL_SCALE - WATER_LEVEL,
				row * CELL_SIZE)));
			_texCoord0.push_back(std::move(Vector2f(col * (1.0f / _width), row * (1.0f / _height))));
			_texCoord1.push_back(std::move(Vector2f(0, 0)));
			_normals.push_back(std::move(Vector3f(0, 1, 0)));

			_vertices.push_back(std::move(Vector3f(
				col * CELL_SIZE + CELL_SIZE,
				data[(col + 1 + row * _width) * 3] * VERTICAL_SCALE - WATER_LEVEL,
				row * CELL_SIZE)));
			_texCoord0.push_back(std::move(Vector2f((col + 1) * (1.0f / _width), row * (1.0f / _height))));
			_texCoord1.push_back(std::move(Vector2f(5.0f, 0)));
			_normals.push_back(std::move(Vector3f(0, 1, 0)));

			_vertices.push_back(std::move(Vector3f(
				col * CELL_SIZE,
				data[(col + (row + 1) * _width) * 3] * VERTICAL_SCALE - WATER_LEVEL,
				row * CELL_SIZE + CELL_SIZE)));
			_texCoord0.push_back(std::move(Vector2f(col * (1.0f / _width), (row + 1) * (1.0f / _height))));
			_texCoord1.push_back(std::move(Vector2f(0.0f, 5.0f)));
			_normals.push_back(std::move(Vector3f(0, 1, 0)));


			_vertices.push_back(std::move(Vector3f(
				col * CELL_SIZE + CELL_SIZE,
				data[(col + 1 + row * _width) * 3] * VERTICAL_SCALE - WATER_LEVEL,
				row * CELL_SIZE)));
			_texCoord0.push_back(std::move(Vector2f((col + 1) * (1.0f / _width), row * (1.0f / _height))));
			_texCoord1.push_back(std::move(Vector2f(5.0f, 0.0f)));
			_normals.push_back(std::move(Vector3f(0, 1, 0)));

			_vertices.push_back(std::move(Vector3f(
				col * CELL_SIZE + CELL_SIZE,
				data[(col + 1 + (row + 1) * _width) * 3] * VERTICAL_SCALE - WATER_LEVEL,
				row * CELL_SIZE + CELL_SIZE)));
			_texCoord0.push_back(std::move(Vector2f((col + 1) * (1.0f / _width), (row + 1) * (1.0f / _height))));
			_texCoord1.push_back(std::move(Vector2f(5.0f, 5.0f)));
			_normals.push_back(std::move(Vector3f(0, 1, 0)));

			_vertices.push_back(std::move(Vector3f(
				col * CELL_SIZE,
				data[(col + (row + 1) * _width) * 3] * VERTICAL_SCALE - WATER_LEVEL,
				row * CELL_SIZE + CELL_SIZE)));
			_texCoord0.push_back(std::move(Vector2f(col * (1.0f / _width), (row + 1) * (1.0f / _height))));
			_texCoord1.push_back(std::move(Vector2f(0.0f, 5.0f)));
			_normals.push_back(std::move(Vector3f(0, 1, 0)));
		}
	}

	stbi_image_free(data);

	GenerateObjectsOnTerrain(*this);

	return true;
}

float Terrain::GetHeight(const Vector3f& position) const
{
	if (position.X < 0 || position.X > _width * CELL_SIZE ||
		position.Z < 0 || position.Z > _height * CELL_SIZE)
	{
		return -9.0;
	}

	int face = ((int)(position.X / CELL_SIZE) + (int)(position.Z / CELL_SIZE) * (_width - 1)) * 2;

	Vector3f bod_A = _vertices[3 * face];
	Vector3f bod_B = _vertices[3 * face + 1];
	Vector3f bod_C = _vertices[3 * face + 2];

	Vector3f u(bod_B - bod_A);
	Vector3f v(bod_C - bod_A);

	return bod_A.Y;

	Vector3f w = u.Cross(v);

	float d = bod_A.Dot(w);

	return (position.X * w.X + position.Z * w.Z + d) / w.Y;
}

int Terrain::quad(float px, float pz)
{
	int quad;

	quad = (int)(px / (CELL_SIZE * 4)) + (int)(pz / (CELL_SIZE * 4)) * 32;

	return quad;
}