#include "HUD.h"
#include "RenderPrimitives.h"
#include "Text.h"
#include <windows.h>
#include <gl\gl.h>


HUD::HUD()
	:
	_speedBackground(Texture::GetTexture("data\\textures\\hud\\SpeedBackground.bmp")),
	textura_zdravi(Texture::GetTexture("data\\textures\\hud\\hud.bmp")),
	textura_kompas(Texture::GetTexture("data\\textures\\hud\\kompas-up.bmp")),
	textura_powered(Texture::GetTexture("data\\textures\\hud\\powered.bmp")),
	textura_mapa(Texture::GetTexture("data\\textures\\mapa.bmp")),
	textura_kurzor(Texture::GetTexture("data\\textures\\hud\\mapa_kur.bmp")),
	textura_podtext(Texture::GetTexture("data\\textures\\hud\\podtext.bmp")),
	textura_zatext(Texture::GetTexture("data\\textures\\hud\\zatext.bmp")),
	textura_logo(Texture::GetTexture("data\\textures\\hud\\top.bmp")),
	textura_info(Texture::GetTexture("data\\textures\\hud\\info.bmp")),
	textura_keen(Texture::GetTexture("data\\textures\\hud\\logo_keensoft.bmp")),
	time("15:29")
{
}

void HUD::Render(const Ship& ship) const
{	
	glMatrixMode(GL_PROJECTION);

	glPushMatrix();

	glLoadIdentity();
	glOrtho(0, 1, 0, 1, -1, 1);
	
	glDisable(GL_LIGHTING);	
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GEQUAL, 1);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	_speedBackground.Bind();

	RenderPrimitives::RenderQuad2D(Vector2f(0,0), Vector2f(0.15f, 0.2f));
	
	//glTranslatef(65, 538, 0);
	//glRotatef(-115 + abs(ship.GetVelocity()), 0, 0.0f, 1.0f);

	//textura_kurzor.Bind();

	//glBegin(GL_QUADS);              /* zacatek zadavani ctverce */
	//glTexCoord2f(0.0, 1.0);
	//glVertex2i(-4, -40);			/* poloha prvniho vertexu */
	//glTexCoord2f(1.0, 1.0);
	//glVertex2i(4, -40);		/* poloha druheho vertexu */
	//glTexCoord2f(1.0, 0.0);
	//glVertex2i(4, 10);/* poloha tretiho vertexu */
	//glTexCoord2f(0.0, 0.0);
	//glVertex2i(-4, 10);		/* poloha ctvrteho vertexu */
	//glEnd();
	//

	//if (keys['R'])
	//{
	//	refrakce = !refrakce;
	//	keys['R'] = false;
	//}

	//if (keys[VK_F2])
	//{
	//	textura_zatext.Bind();

	//	glBegin(GL_QUADS);              /* zacatek zadavani ctverce */
	//	glTexCoord2f(0.0, 0.6);
	//	glVertex2i(5, 5);			/* poloha prvniho vertexu */
	//	glTexCoord2f(1.0, 0.6);
	//	glVertex2i(200, 5);		/* poloha druheho vertexu */
	//	glTexCoord2f(1.0, 0.0);
	//	glVertex2i(200, 72);/* poloha tretiho vertexu */
	//	glTexCoord2f(0.0, 0.0);
	//	glVertex2i(5, 72);		/* poloha ctvrteho vertexu */
	//	glEnd();

	//	char fpsko[20];
	//	sprintf(fpsko, "FPS : %f", 1 / deltaT);
	//	printText(fpsko, font_12, 10, 20, 0.0, 0.0, 0.0);

	//	sprintf(fpsko, "GPS X : %f", pozice_x);
	//	printText(fpsko, font_12, 10, 35, 0.0, 0.0, 0.0);

	//	sprintf(fpsko, "GPS Y : %f", pozice_z);
	//	printText(fpsko, font_12, 10, 50, 0.0, 0.0, 0.0);

	//	sprintf(fpsko, "Vyska : %f", hloubka);
	//	printText(fpsko, font_12, 10, 65, 0.0, 0.0, 0.0);
	//}

	//if (near_veh)
	//{
	//	glEnable(GL_BLEND);

	//	textura_podtext.Bind();

	//	glBegin(GL_QUADS);              /* zacatek zadavani ctverce */
	//	glTexCoord2f(0.0, 1.0);
	//	glVertex2i(230, 350);			/* poloha prvniho vertexu */
	//	glTexCoord2f(1.0, 1.0);
	//	glVertex2i(460, 350);		/* poloha druheho vertexu */
	//	glTexCoord2f(1.0, 0.0);
	//	glVertex2i(460, 380);/* poloha tretiho vertexu */
	//	glTexCoord2f(0.0, 0.0);
	//	glVertex2i(230, 380);		/* poloha ctvrteho vertexu */
	//	glEnd();
	//	glDisable(GL_BLEND);
	//	printText("Nastoupit do vozidla (klavesa \"F\")", font_24, 250, 370, 0.0, 0.5, 0.8);
	//}

	//if (keys[VK_TAB])
	//{
	//	glBlendFunc(GL_DST_COLOR, GL_SRC_COLOR); //Typ blendingu 
	//	//glEnable(GL_BLEND);

	//	textura_mapa.Bind();

	//	glBegin(GL_QUADS);              /* zacatek zadavani ctverce */
	//	glTexCoord2f(0.0, 0.0);
	//	glVertex2i(144, 44);			/* poloha prvniho vertexu */
	//	glTexCoord2f(1.0, 0.0);
	//	glVertex2i(656, 44);		/* poloha druheho vertexu */
	//	glTexCoord2f(1.0, 1.0);
	//	glVertex2i(656, 556);		/* poloha tretiho vertexu */
	//	glTexCoord2f(0.0, 1.0);
	//	glVertex2i(144, 556);			/* poloha ctvrteho vertexu */
	//	glEnd();
	//	glDisable(GL_BLEND);

	//	int poloha_x = (int)(2 * pozice_x / 32);
	//	int poloha_y = (int)(2 * pozice_z / 32);

	//	glPushMatrix();

	//	glTranslatef(144 + poloha_x, 44 + poloha_y, 0);
	//	glRotatef(scenerotx, 0, 0.0f, 1.0f);

	//	textura_kurzor.Bind();

	//	glBegin(GL_QUADS);              /* zacatek zadavani ctverce */
	//	glTexCoord2f(0.0, 1.0);
	//	glVertex2i(-8, -8);			/* poloha prvniho vertexu */
	//	glTexCoord2f(1.0, 1.0);
	//	glVertex2i(8, -8);		/* poloha druheho vertexu */
	//	glTexCoord2f(1.0, 0.0);
	//	glVertex2i(8, 8);/* poloha tretiho vertexu */
	//	glTexCoord2f(0.0, 0.0);
	//	glVertex2i(-8, 8);		/* poloha ctvrteho vertexu */
	//	glEnd();
	//	glPopMatrix();
	//}

	//if (keys['G'])
	//{
	//	textura_zatext.Bind();

	//	glBegin(GL_QUADS);              /* zacatek zadavani ctverce */
	//	glTexCoord2f(0.0, 0.0);
	//	glVertex2i(50, 110);			/* poloha prvniho vertexu */
	//	glTexCoord2f(1.0, 0.0);
	//	glVertex2i(350, 110);		/* poloha druheho vertexu */
	//	glTexCoord2f(1.0, 1.0);
	//	glVertex2i(350, 50);		/* poloha tretiho vertexu */
	//	glTexCoord2f(0.0, 1.0);
	//	glVertex2i(50, 50);			/* poloha ctvrteho vertexu */
	//	glEnd();
	//	printText(goal_1, font_12, 60, 70, 0.2, 0.2, 0.2);
	//	printText(goal_2, font_12, 60, 85, 0.2, 0.2, 0.2);
	//	printText(goal_3, font_12, 60, 100, 0.2, 0.2, 0.2);
	//}

	textura_info.Bind();

	RenderPrimitives::RenderQuad2D(Vector2f(0.85f, 0.9f), Vector2f(0.15f, 0.1f));

	glDisable(GL_ALPHA_TEST);
	
	//RenderText("Ahoj", Vector2f(0.3f, 0.3f), Color(0, 1, 0));

	//glBegin(GL_QUADS);              /* zacatek zadavani ctverce */
	//glTexCoord2f(0.0, 1.0);
	//glVertex2i(720, 0);		/* poloha prvniho vertexu */
	//glTexCoord2f(1.0, 1.0);
	//glVertex2i(800, 0);		/* poloha druheho vertexu */
	//glTexCoord2f(1.0, 0);
	//glVertex2i(800, 40);		/* poloha tretiho vertexu */
	//glTexCoord2f(0.0, 0);
	//glVertex2i(720, 40);			/* poloha ctvrteho vertexu */
	//glEnd();

	//mise.time -= deltaT;

	//time = new char[4];
	//int timek = (int)mise.time;

	///*if (timek%60>9)
	//	sprintf(time.c_str(), "%i:%i", timek / 60, timek % 60);

	//else
	//	sprintf(time.c_str(), "%i:0%i", timek / 60, timek % 60);*/

	//if (timek > 30) printText(time, font_12, 750, 18, 0, 0, 0);
	//else printText(time, font_12, 750, 18, 1, 0, 0);
	//char* zdravicko;
	//zdravicko = new char[5];
	//sprintf(zdravicko, "%i%%", scene->jachta.zdravi);
	//if (scene->jachta.zdravi > 30) printText(zdravicko, font_12, 750, 36, 0, 0, 0);
	//else printText(zdravicko, font_12, 750, 36, 1, 0, 0);

	//glPushMatrix();
	//glTranslatef(166, 561, 0);
	//glRotatef(-scenerotx, 0, 0.0f, 1.0f);

	//textura_kompas.Bind();

	//glBegin(GL_QUADS);              /* zacatek zadavani ctverce */
	//glTexCoord2f(0.0, 1.0);
	//glVertex2i(-40, -40);			/* poloha prvniho vertexu */
	//glTexCoord2f(1.0, 1.0);
	//glVertex2i(40, -40);		/* poloha druheho vertexu */
	//glTexCoord2f(1.0, 0.0);
	//glVertex2i(40, 40);/* poloha tretiho vertexu */
	//glTexCoord2f(0.0, 0.0);
	//glVertex2i(-40, 40);		/* poloha ctvrteho vertexu */
	//glEnd();
	//glEnable(GL_DEPTH_TEST);
	//glPopMatrix();

	//if ((scene->jachta.pozice.X < -500) || (scene->jachta.pozice.X > 8692) ||
	//	(scene->jachta.pozice.Z < -500) || (scene->jachta.pozice.Z > 8692))
	//{
	//	printText("Opou�t�te hern� �zem�,", font_24, 300, 280, 1, 0, 0);
	//	printText("     ihned se oto�te", font_24, 300, 310, 1, 0, 0);
	//}

	//if ((scene->jachta.pozice.X < -2000) || (scene->jachta.pozice.X > 10192) ||
	//	(scene->jachta.pozice.Z < -2000) || (scene->jachta.pozice.Z > 10192))
	//	scene->jachta.ext_kolize = true;

	glMatrixMode(GL_PROJECTION);

	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
}

