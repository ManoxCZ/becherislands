/**************************************************************/
/* File : Max_Ship.cpp										  */
/* Developed : Manox (manox@atlas.cz)						  */
/* Date : 5.8.2005											  */
/**************************************************************/

#include "Max_Math.h"
#include "Ship.h"
#include <windows.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include <gl\glut.h>


Ship::Ship()
	:
	_model(Model::GetModel("data\\models\\powerboat.obj", "")),
	_position(1000, 0, 500),
	_angles(0, 0, 0),
	_engineState(EngineState::Offline),
	_maxVelocity(200),
	_acceleration(400),
	_velocity(0),
	_smoke(1, Color(0.2f, 0.2f, 0.2f), Color(1.0f, 1.0f, 1.0f), 0.3f, 2, 200),
	_wavesTime(0)
{
	_smoke.Position = Vector3f(1, 5, -7);
	_smoke.Velocity = Vector3f(0, 5, 0);
}

Ship::~Ship()
{

}

void Ship::init()
{		
	kanal = 1;
	
	_smoke.Initialize();

	start.Load("data/sounds/boat/boat_start.wav");
	idle.Load("data/sounds/boat/boat_idle.wav");
	end.Load("data/sounds/boat/boat_end.wav");
	trubkas.Load("data/sounds/boat/trubka.wav");
	crash.Load("data/sounds/boat/crash.wav");
	/*start = FSOUND_Sample_Load(FSOUND_FREE, "data/sounds/boat/boat_start.wav", 
		FSOUND_NORMAL | FSOUND_HW2D, 0, 0);

	idle = FSOUND_Sample_Load(FSOUND_FREE, "data/sounds/boat/boat_idle.wav", 
		FSOUND_NORMAL | FSOUND_HW2D, 0, 0);
	FSOUND_Sample_SetMode(idle, FSOUND_LOOP_NORMAL);

	end = FSOUND_Sample_Load(FSOUND_FREE, "data/sounds/boat/boat_end.wav", 
		FSOUND_NORMAL | FSOUND_HW2D, 0, 0);

	trubkas = FSOUND_Sample_Load(FSOUND_FREE, "data/sounds/boat/trubka.wav", 
		FSOUND_NORMAL | FSOUND_HW2D, 0, 0);

	crash = FSOUND_Sample_Load(FSOUND_FREE, "data/sounds/boat/crash.wav", 
		FSOUND_NORMAL | FSOUND_HW2D, 0, 0);*/

	oldpozice.push_back(_position);
	oldpozice.push_back(_position);
	olduhel.push_back(_angles);
	olduhel.push_back(_angles);
}

void Ship::reset()
{
	_engineState = EngineState::Offline;	
	kolize = false;
	ext_kolize = false; 
	zdravi = 100;
	_velocity = 0;
	_smoke.Reset();		
}

void Ship::Update(float deltaTime, const Input& input)
{
	_wavesTime += deltaTime * 10;

	float nejm_x = -5.45916f;
	float nejm_y = -26.2655f;
	float nejv_x = 3.795f;
	float nejv_y = 4.02911f;

	kol_A = Vector2f(
		_position.X + nejm_x * cos(-DegToRad(_angles.Y)) - nejm_y * sin(-DegToRad(_angles.Y)),
		_position.Z + nejm_x * sin(-DegToRad(_angles.Y)) + nejm_y * cos(-DegToRad(_angles.Y)));

	kol_B = Vector2f(
		_position.X + nejv_x * cos(-DegToRad(_angles.Y)) - nejm_y * sin(-DegToRad(_angles.Y)),
		_position.Z + nejv_x * sin(-DegToRad(_angles.Y)) + nejm_y * cos(-DegToRad(_angles.Y)));

	kol_C = Vector2f(
		_position.X + nejv_x * cos(-DegToRad(_angles.Y)) - nejv_y * sin(-DegToRad(_angles.Y)),
		_position.Z + nejv_x * sin(-DegToRad(_angles.Y)) + nejv_y * cos(-DegToRad(_angles.Y)));

	kol_D = Vector2f(
		_position.X + nejm_x * cos(-DegToRad(_angles.Y)) - nejv_y * sin(-DegToRad(_angles.Y)),
		_position.Z + nejm_x * sin(-DegToRad(_angles.Y)) + nejv_y * cos(-DegToRad(_angles.Y)));



	switch (_engineState)
	{
	case EngineState::Offline:
		UpdateOffline(deltaTime, input);
		break;
	case EngineState::Starting:
		UpdateStarting(deltaTime, input);
		break;
	case EngineState::Running:
		UpdateRunning(deltaTime, input);
		break;
	case EngineState::Stopping:
		UpdateStopping(deltaTime, input);
		break;
	}

	UpdateRudder(deltaTime, input);

	UpdateThrottle(deltaTime, input);

	///*for (unsigned int i = 0; i < doky.size(); i++)
	//	doky[i].prepocet();*/

	////if ((teren.height(pozice.X, pozice.Z) > -3) || (ext_kolize))
	////{
	////	crash.Play();
	////	//FSOUND_PlaySound(3, crash);
	////	kolize = true;
	////}

	//if ((!kolize) && (!ext_kolize))
	//{
	//	oldpozice.push_back(_position);
	//	oldpozice.pop_front();
	//	olduhel.push_back(_angles);
	//	olduhel.pop_front();
	//}
	//else
	//{		
	//	_forceIntegrator.hodnota = 0;
	//	_speedIntegrator.hodnota = 0;
	//	_position = oldpozice[0];
	//	_angles = olduhel[0];
	//	kolize = false;
	//	ext_kolize = false;
	//	zdravi -= abs(_velocity) / 5;
	//	_velocity = 0;
	//}
	//
	//if (keys['H'] && !trubka)
	//{
	//	trubkas.Play();
	//	//FSOUND_PlaySound(FSOUND_FREE, trubkas);
	//	trubka = true;
	//}
	///*else if (FSOUND_IsPlaying(3) == false)
	//	trubka = false;*/

	//_smoke.Update(deltaTime);

	/*_position.X += deltaTime * _velocity * (float)sin(0.0174532925f * _angles.Y);
	_position.Z += deltaTime * _velocity * (float)cos(0.0174532925f * _angles.Y);*/

	_camera.Position = _position + Vector3f(
		sin(0.0174532925f * _angles.Y) * -10,
		4,
		cos(0.0174532925f * _angles.Y) * -10);

	_camera.Target = _position + Vector3f(0, 2, 0);
}

void Ship::UpdateOffline(float deltaTime, const Input& input)
{
	if (input.IsKeyPressed('e'))
	{
		start.Play();
		
		_smoke.SpawnRatio = 10;

		_engineState = EngineState::Running;
	}
}

void Ship::UpdateStarting(float deltaTime, const Input& input)
{
	if (!start.IsPlaying())		
	{
		_engineState = EngineState::Running;
		/*	FSOUND_StopSound(kanal);
			FSOUND_PlaySound(kanal, idle);*/
		
		//zak_frek = FSOUND_GetFrequency(kanal);
		/*frekvence = zak_frek;
		FSOUND_SetVolume(kanal,250);*/
	}
}

void Ship::UpdateRunning(float deltaTime, const Input& input)
{
	/*if (keys['E'])
	{
		end.Play();

		_smoke.SpawnRatio = 0;

		_engineState = EngineState::Stopping;
	}*/
}

void Ship::UpdateStopping(float deltaTime, const Input& input)
{
	if (!end.IsPlaying())
	{
		_engineState = EngineState::Offline;		
	}
}

void Ship::UpdateRudder(float deltaTime, const Input& input)
{
	if (input.IsKeyPressed('a') || input.IsSpecialKeyPressed(GLUT_KEY_LEFT))
	{
		if (abs(_velocity) > 0.5)
		{
			_angles.Y += 30 * deltaTime * abs(_velocity / _maxVelocity * 10);
		}
	}

	if (input.IsKeyPressed('d') || input.IsSpecialKeyPressed(GLUT_KEY_RIGHT))
	{
		if (abs(_velocity) > 0.5)
		{
			_angles.Y -= 30 * deltaTime * abs(_velocity / _maxVelocity * 10);
		}
	}
}

void Ship::UpdateThrottle(float deltaTime, const Input& input)
{
	float acceleration = 0;

	if ((input.IsKeyPressed('w') || input.IsSpecialKeyPressed(GLUT_KEY_UP)) && _engineState == EngineState::Running)
	{
		acceleration = _acceleration;
	}
	else if ((input.IsKeyPressed('d') || input.IsSpecialKeyPressed(GLUT_KEY_DOWN)) && _engineState == EngineState::Running)
	{
		acceleration = - _acceleration;
	}
	else
	{
		acceleration = -_velocity;
	}

	_velocity = std::min(_maxVelocity, _velocity + deltaTime * acceleration);
	
	_position.X += deltaTime * _velocity * (float)sin(0.0174532925f * _angles.Y);
	_position.Z += deltaTime * _velocity * (float)cos(0.0174532925f * _angles.Y);
}

void Ship::Render(const Camera& camera) const
{
	glLoadIdentity();

	camera.SetLookAt();

	glTranslatef(_position.X, _position.Y, _position.Z);
	
	glRotatef(_angles.X + sin(_wavesTime) * 2, 1, 0, 0);
	glRotatef(_angles.Y, 0, 1, 0);
	glRotatef(_angles.Z + cos(_wavesTime + 10) * 2, 0, 0, 1);

	_model.Render();

	//_smoke.Render();

	//glPopMatrix();
/*	glPushMatrix();

	glTranslatef(pozice.x, pozice.y, pozice.z);
	glRotatef(uhel.x,1,0,0);
	glRotatef(uhel.y,0,1,0);
	glRotatef(uhel.z,0,0,1);

	glBindTexture(GL_TEXTURE_2D, textury[newWave.textura]);
	glBegin(GL_QUADS);
		glTexCoord2f(0.0f,1.0f);
		glVertex3f(-2.5,0.1,0.5);
		glTexCoord2f(0.0f,0.0f);
		glVertex3f(-5.5,0.1,0.5);
		glTexCoord2f(5.0f,0.0f);
		glVertex3f(-12.5,0.1,150*-rychlost/max_rychlost);
		glTexCoord2f(5.0f,1.0f);
		glVertex3f(-2.5,0.1,150*-rychlost/max_rychlost);

		glTexCoord2f(0.0f,1.0f);
		glVertex3f(1.5,0.1,-0.5);
		glTexCoord2f(0.0f,0.0f);
		glVertex3f(4.5,0.1,-0.5);
		glTexCoord2f(5.0f,0.0f);
		glVertex3f(11.5,0.1,150*-rychlost/max_rychlost-0.5);
		glTexCoord2f(5.0f,1.0f);
		glVertex3f(1.5,0.1,150*-rychlost/max_rychlost-0.5);

		glTexCoord2f(0.0f,1.0f);
		glVertex3f(0,0.1,0);
		glTexCoord2f(0.0f,0.0f);
		glVertex3f(0,0.1,0);
		glTexCoord2f(5.0f,0.0f);
		glVertex3f(-5,0.1,150*-rychlost/max_rychlost);
		glTexCoord2f(5.0f,1.0f);
		glVertex3f(0,0.1,150*-rychlost/max_rychlost);

		glTexCoord2f(0.0f,1.0f);
		glVertex3f(0,0.1,0);
		glTexCoord2f(0.0f,0.0f);
		glVertex3f(0,0.1,0);
		glTexCoord2f(5.0f,0.0f);
		glVertex3f(5,0.1,150*-rychlost/max_rychlost);
		glTexCoord2f(5.0f,1.0f);
		glVertex3f(0,0.1,150*-rychlost/max_rychlost);
	glEnd();

	glPopMatrix();*/
	/*
		glDisable(GL_LIGHTING);
		glColor3f(1,0,0);

		glBegin(GL_LINES);
			glVertex3f(kol_A[0], 5, kol_A[1]);
			glVertex3f(kol_B[0], 5, kol_B[1]);
			glVertex3f(kol_B[0], 5, kol_B[1]);
			glVertex3f(kol_C[0], 5, kol_C[1]);
			glVertex3f(kol_C[0], 5, kol_C[1]);
			glVertex3f(kol_D[0], 5, kol_D[1]);
			glVertex3f(kol_D[0], 5, kol_D[1]);
			glVertex3f(kol_A[0], 5, kol_A[1]);
		glEnd();
		glEnable(GL_LIGHTING);
		glColor3f(1,1,1);*/
}
