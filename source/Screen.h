#pragma once

#include "Button.h"
#include "Camera.h"
#include "Input.h"
#include "MenuAction.h"
#include <vector>


enum class ScreenId
{
	MainMenu = 0,
};


class Screen
{
private:
	bool _backButtonPressed;
	bool _nextButtonPressed;
	bool _actionButtonPressed;

public:	
	std::vector<Button> Buttons;

	Screen();

	MenuAction Update(float deltaTime, const Input& input);
	void Render(const Camera& camera) const;

private:
	void UpdateKeyboardInput(const Input& input, MenuAction& action);
	void MoveBack();
	void MoveNext();
	int GetActiveButton() const;
};
