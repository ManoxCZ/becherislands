#pragma once

//#include "al.h"
#include <string>

class Sound
{
	//char*     alBuffer;             //data for the buffer
	//ALenum alFormatBuffer;			//buffer format
	//ALsizei   alFreqBuffer;			//frequency
	//long       alBufferLen;			//bit depth
	//ALboolean    alLoop;			//loop
	//unsigned int alSource;			//source
	//unsigned int alSampleSet;

public:
	Sound();
	~Sound();

	bool Load(const std::string& filename);
	void Play();
	void Stop();
	bool IsPlaying() const;
};