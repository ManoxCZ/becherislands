/**************************************************************/
/* File : Max_Ship.h										  */
/* Developed : Manox (manox@atlas.cz)						  */
/* Date : 5.8.2005											  */
/**************************************************************/

#ifndef _SHIP_H
#define _SHIP_H

#include "Camera.h"
#include "Input.h"
#include "Sound.h"
#include "Vector.h"
#include "Particle_Emiter.h"
#include "Model.h"
#include <deque>


enum class EngineState
{
	Offline,
	Starting,
	Running,
	Stopping,
};

class Ship
{
private:
	Camera _camera;
	const Model& _model;
	ParticleEmitter _smoke;
	EngineState _engineState;
	float _acceleration;
	float _velocity;
	float _maxVelocity;
	Vector3f _position;
	Vector3f _angles;
	double _wavesTime;

public:	
	std::deque <Vector3f> oldpozice;	
	std::deque <Vector3f> olduhel;	
	bool trubka;
	float frekvence;
	float zak_frek;
	
	bool kolize;
	bool ext_kolize;
	Vector2f kol_A;
	Vector2f kol_B;
	Vector2f kol_C;
	Vector2f kol_D;
	int zdravi;

	unsigned int kanal;
	Sound start;
	Sound idle;
	Sound end;
	Sound trubkas;
	Sound crash;

	Ship();
	~Ship();

	void init();
	void reset();

	void Update(float deltaTime, const Input& input);
	void Render(const Camera& camera) const;
	
	Camera& GetCamera() { return _camera; }
	float GetVelocity() const { return _velocity; }

private:
	void UpdateOffline(float deltaTime, const Input& input);
	void UpdateStarting(float deltaTime, const Input& input);
	void UpdateRunning(float deltaTime, const Input& input);
	void UpdateStopping(float deltaTime, const Input& input);
	void UpdateRudder(float deltaTime, const Input& input);
	void UpdateThrottle(float deltaTime, const Input& input);
};

#endif